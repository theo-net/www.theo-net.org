www.theo-net.org
================

Site web principal.

Lancez pour unitialiser le repo : 

   npm install

Pour charger la base de données : 

   mysql -u myUser --password=myPassword myDb --default-character-set=utf8mb4  < ./entity/db.sql


Pour les tests, utilisez : 

   npm run nodemon

Pour la production : 

    pm2 start ./server.js --name tn_network --  start

Pour gérer le démarage automatique après un reboot : 

   https://pm2.keymetrics.io/docs/usage/startup/

Pour charger une sauvegarde LDAP : 

   sudo service slapd stop
   sudo slapadd -c -b dc=example,dc=com -l Export-Example-com.ldif
