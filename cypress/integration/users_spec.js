describe('Connection', function () {

  Cypress.config('baseUrl', 'http://www.theo-net.local:8090') ;

  it('DuckDuckGo', function () {

    cy.visit('https://duckduckgo.com') ;
    cy.get('#search_form_input_homepage').type('Théo-Net', {force: true}) ;
    cy.get('#search_button_homepage').click({force: true}) ;
    cy.get('#web_content_wrapper').contains('Accueil - Théo-Net') ;
  }) ;


  describe('teste la validité des champs', function () {

    it('erreur si email non reconnu', function () {

      cy.visit('/connection.html') ;

      cy.get('#mail').type('demofoobar@theo-net.org') ;
      cy.get('#password').type('foobar') ;
      cy.get('form').submit() ;

      cy.get('#user_flash').contains('aucun utilisateur associé à ce mail') ;
    }) ;

    it('erreur si password invalide', function () {

      cy.visit('/connection.html') ;

      cy.get('#mail').type('demo@theo-net.org') ;
      cy.get('#password').type('foobar') ;
      cy.get('form').submit() ;
    
      cy.get('#user_flash').contains('identifiants sont sans doute incorrects') ;
    }) ;

    it('connexion, redirection, new session si tout est bon', function () {

      cy.visit('http://www.theo-net.local:8090/connection.html') ;
      let sessionId = '' ;
      cy.getCookie('tn_session_id').should(cookie => {
        sessionId = cookie.value ;
      }) ;

      cy.get('#mail').type('demo@theo-net.org') ;
      cy.get('#password').type('demodemo') ;
      cy.get('form').submit() ;
   
      cy.url().should('eq', 'http://www.theo-net.local:8090/') ;
      cy.get('#user_flash').contains('Vous êtes bien connectés') ;
      cy.get('.NetworkBar__user').contains('démo') ;

      cy.getCookie('tn_session_id').should(cookie => {
        expect(cookie.value).to.be.not.equal(sessionId) ; 
      }) ;
    }) ;

    it('redirection pour admin et kepha ok', function () {

      cy.visit('http://admin.theo-net.local:8090/connection.html') ;
      cy.get('#mail').type('demo@theo-net.org') ;
      cy.get('#password').type('demodemo') ;
      cy.get('form').submit() ;
   
      cy.url().should('eq', 'http://admin.theo-net.local:8090/') ;

      cy.visit('http://kepha.theo-net.local:8090/connection.html') ;
      cy.get('#mail').type('demo@theo-net.org') ;
      cy.get('#password').type('demodemo') ;
      cy.get('form').submit() ;
   
      cy.url().should('eq', 'http://kepha.theo-net.local:8090/') ;
    }) ;
  }) ;


  describe('persistence de la connexion', function () {

    it('on peut rester connecté en navigant', function () {

      cy.visit('/') ;
      cy.request('POST', '/connection.html', 'mail=demo@theo-net.org&password=demodemo')
      .then(response => {
        expect(response.body).contains('démo') ;
      }) ;
      cy.request('GET', '/apropos.html')
      .then(response => {
        expect(response.body).contains('démo') ;
      }) ;
    }) ;

    it('deconnexion', function () {

      cy.visit('/') ;
      cy.request('POST', '/connection.html', 'mail=demo@theo-net.org&password=demodemo')
      .then(response => {
        expect(response.body).contains('démo') ;
      }) ;
      cy.request('GET', '/disconnection.html').then(response => {
        expect(response.body).contains('Vous êtes bien déconnectés') ;
      }) ;
      cy.request('GET', '/apropos.html')
      .then(response => {
        expect(response.body).contains('Se connecter') ;
      }) ;
    }) ;

    it('connexion permanente', function () {

      let persistToken = '' ; 

      cy.visit('/') ;
      cy.request(
        'POST', '/connection.html', 'mail=demo@theo-net.org&password=demodemo&persist=true'
      ).then(response => {
        expect(response.body).contains('démo') ;
      
        cy.getCookie('tn_persist').should(cookie => {
          persistToken = cookie.value ;
        
          cy.clearCookies() ;
          cy.request('GET', '/apropos.html')
          .then(response => {
            expect(response.body).contains('Se connecter') ;
          }) ;
          
          cy.setCookie('tn_persist', persistToken) ;
          cy.getCookie('tn_persist').should(cookie => {
            persistToken = cookie.value ;
          }) ;
          cy.request('GET', '/apropos.html')
          .then(response => {
            expect(response.body).contains('démo') ;
          }) ;
        }) ;
      }) ;
    }) ;
  }) ;
}) ;

