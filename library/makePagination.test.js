'use strict' ;

/**
 * library/makePAgination.test.js
 */

const expect = require('chai').expect ;

const makePagination = require('./makePagination') ;

describe('library/makePagination()', function () {

  it('ne retourne pas de pagination si 0 ou 1 page', function () {

    expect(makePagination({total: 0})).to.be.equal('') ;
    expect(makePagination({total: 1})).to.be.equal('') ;
  }) ;

  it('sinon génère une pagination', function () {

    let pagination = {
      link: 'page-{{num}}.html',
      total: 4,
      current: 2
    } ;

    expect(makePagination(pagination)).to.be.equal(
      '<nav class="mtm mbm txtcenter">'
      + '<ul class="pagination">'
      + '<li class="active"><a href="page-1.html">'
      + '<i class="icon icon-chevron-left"></i></a></li>'
      + '<li><a href="page-1.html">1</a></li>'
      + '<li class="active"><a href="page-2.html">2</a></li>'
      + '<li><a href="page-3.html">3</a></li>'
      + '<li><a href="page-4.html">4</a></li>'
      + '<li class="active"><a href="page-3.html">'
      + '<i class="icon icon-chevron-right"></i></a></li>'
      + '</ul></nav>'
    ) ;
  }) ;

  it('Si première ou dernière page, désactive flèches', function () {

    let pagination = {
      link: 'page-{{num}}.html',
      total: 4,
      current: 1
    } ;
    expect(makePagination(pagination)).to.be.equal(
      '<nav class="mtm mbm txtcenter">'
      + '<ul class="pagination">'
      + '<li class="disabled"><a href="page-1.html">'
      + '<i class="icon icon-chevron-left"></i></a></li>'
      + '<li class="active"><a href="page-1.html">1</a></li>'
      + '<li><a href="page-2.html">2</a></li>'
      + '<li><a href="page-3.html">3</a></li>'
      + '<li><a href="page-4.html">4</a></li>'
      + '<li class="active"><a href="page-2.html">'
      + '<i class="icon icon-chevron-right"></i></a></li>'
      + '</ul></nav>'
    ) ;

    pagination = {
      link: 'page-{{num}}.html',
      total: 4,
      current: 4
    } ;
    expect(makePagination(pagination)).to.be.equal(
      '<nav class="mtm mbm txtcenter">'
      + '<ul class="pagination">'
      + '<li class="active"><a href="page-3.html">'
      + '<i class="icon icon-chevron-left"></i></a></li>'
      + '<li><a href="page-1.html">1</a></li>'
      + '<li><a href="page-2.html">2</a></li>'
      + '<li><a href="page-3.html">3</a></li>'
      + '<li class="active"><a href="page-4.html">4</a></li>'
      + '<li class="disabled"><a href="">'
      + '<i class="icon icon-chevron-right"></i></a></li>'
      + '</ul></nav>'
    ) ;
  }) ;

  it('on peut forcer le lien vers la première page', function () {

    let pagination = {
      first: 'first.html',
      link: 'page-{{num}}.html',
      total: 4,
      current: 2
    } ;
    expect(makePagination(pagination)).to.be.equal(
      '<nav class="mtm mbm txtcenter">'
      + '<ul class="pagination">'
      + '<li class="active"><a href="first.html">'
      + '<i class="icon icon-chevron-left"></i></a></li>'
      + '<li><a href="first.html">1</a></li>'
      + '<li class="active"><a href="page-2.html">2</a></li>'
      + '<li><a href="page-3.html">3</a></li>'
      + '<li><a href="page-4.html">4</a></li>'
      + '<li class="active"><a href="page-3.html">'
      + '<i class="icon icon-chevron-right"></i></a></li>'
      + '</ul></nav>'
    ) ;

    pagination = {
      first: 'first.html',
      link: 'page-{{num}}.html',
      total: 4,
      current: 3
    } ;
    expect(makePagination(pagination)).to.be.equal(
      '<nav class="mtm mbm txtcenter">'
      + '<ul class="pagination">'
      + '<li class="active"><a href="page-2.html">'
      + '<i class="icon icon-chevron-left"></i></a></li>'
      + '<li><a href="first.html">1</a></li>'
      + '<li><a href="page-2.html">2</a></li>'
      + '<li class="active"><a href="page-3.html">3</a></li>'
      + '<li><a href="page-4.html">4</a></li>'
      + '<li class="active"><a href="page-4.html">'
      + '<i class="icon icon-chevron-right"></i></a></li>'
      + '</ul></nav>'
    ) ;
  }) ;
}) ;

