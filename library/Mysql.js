'use strict' ; 

/**
 * library/Mysql.js
 */

const mysql = require('mysql') ;

/**
 * Gère une connection à un serveur MySQL
 */
class Mysql {

  /**
   * Connection à un serveur MySQL et créé le cannal d'events `mysql`
   * @param {String} host Server MySQL ex : `localhost`
   * @param {String} user Utilisateur
   * @param {String} password Mot de passe
   * @param {String} database Base de données
   * @param {Kernel} kernel Kernel
   */
  constructor (host, user, password, database, kernel) {

    this._kernel = kernel ;

    // Créé un event et attache les logs
    this._kernel.events().register('mysql') ;
    this._kernel.events().attach('mysql', 'log', data => {

      this._kernel.container().get('$logs').log(data.data, data.type) ;
    }) ;

    // Connexion
    this._config = {host, user, password, database} ;
    this._connect() ;
  }


  /**
   * Exécute une requête SQL et renvoit son résultat dans une Promise
   * @param {String} sql Requête SQL
   * @param {Array} [values] Valeurs à insérer dans la requête (à la place 
   *    de `?`). Elles seront échappées pour éviter les injections MySQL.
   * @returns {Promise}
   */
  query (sql, values) {

    return new Promise((resolve, reject) => {

      this._connection.query(sql, values, (error, results) => {
      
        this._kernel.events().notify(
          'mysql', {type: 'data', data: 'Requête : ' + sql}
        ) ;
       
        if (error)
          reject('Erreur dans la requête ' + sql + '\n' + error)  ;
        else
          resolve(results) ;
      }) ;
    }) ;
  }


  /**
   * Connexion à la base de données. En cas de déconnection, une reconnexion
   * est tentée automatiquepent.
   * @private
   */
  _connect () {

    this._connection = mysql.createConnection(this._config) ;
    this._connection.connect(error => {
      if (error) {
        this._kernel.events().notify(
          'mysql', {
            type: 'error', 
            data: 'Error connection db mysql://' 
                  + this._config.user + '@' + this._config.host + '/' 
                  + this._config.database + ' : ' + error
          }
        ) ;
        setTimeout(this._connect, 2000) ;
      }
    }) ;

    this._connection.on('error', error =>  {
        
      this._kernel.events().notify(
        'mysql', {
          type: 'error', 
          data: 'Database MySQL error ' + error
        }
      ) ;

      if (error.code === 'PROTOCOL_CONNECTION_LOST') {
      
        this._kernel.events().notify(
          'mysql', {
            type: 'error', 
            data: 'Database MySQL connection lost'
          }
        ) ;
        this._connect() ;
      }
      else
        throw error ;
    }) ;

    this._kernel.events().notify(
      'mysql', {
        type: 'info', 
        data: 'Connexion au serveur mysql://' 
              + this._config.user + '@' + this._config.host + '/' 
              + this._config.database 
      }
    ) ;
  }
}

module.exports = Mysql ;

