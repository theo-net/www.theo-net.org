'use strict' ;

/**
 * library/Maintenance.js
 */

const fs = require('fs') ;

const Router = require('../kephajs/src/server/Services/Router'),
      Route  = require('../kephajs/src/server/Services/Route') ;

/**
 * Service gérant le mode maintenance
 */
class Maintenance {

  /**
   * Initialise le service
   * @param {String} file Chemin vers le fichier indiquant l'activation ou non
   *    du mode maintenance
   * @param {Kernel} kernel Le Kernel
   */
  constructor (file, kernel) {

    this._kernel = kernel ;
    this._file = file ;

    this._oldRouter = kernel.get('$router') ;
    this._router = new Router() ;
    this._router._routes.setMaintenance = 
      this._oldRouter._routes.adminSetMaintenance ;
    this._router._routes.static = new Route(
      'get', '/{file}', ['@static'],
      {file: '((?:(?:css)|(?:fonts)|(?:img)|(?:js))/.*)'}
    ) ;
    this._router._routes.maintenance = new Route(
      ['get', 'post'],
      '{uri}', ['maintenance', 'index'],
      {uri: '.*'}
    ) ;

    if (fs.existsSync(file))
      this.activate(fs.readFileSync(file).toString()) ;
    else 
      this.desactivate ;
  }

  /**
   * Active le mode maintenance
   * @param {String} message Message particulier à afficher
   */
  activate (message = '') {

    this._active = true ;
    this._message = message ;

    this._oldRouter = this._kernel.get('$router') ;
    this._kernel.container().set('$router', this._router) ;

    fs.writeFileSync(this._file, message) ;
  }


  /**
   * Désactive le mode maintenance
   */
  desactivate () {
  
    this._active = false ;
    this._message = '' ;
    this._kernel.container().set('$router', this._oldRouter) ;
    
    if (fs.existsSync(this._file))
      fs.unlinkSync(this._file) ;
  }


  /**
   * Indique si le mode maintenance est actif ou non
   * @returns {Boolean}
   */
  isActive () {

    return this._active ;
  }


  /**
   * Retourne le message à afficher
   * @returns {String}
   */
  getMessage () {

    return this._message ;
  }
}

module.exports = Maintenance ; 

