'use strict' ;

/**
 * src/lint.spec.js
 */

let lint = require('mocha-eslint') ;

// Note: a seperate Mocha test will be run for each path and each file which
// matches a glob pattern
let paths = [
  '*.js',
  'bin/*.js',
  'controller/**/*.js',
  'entity/**/*.js',
  'form/**/*.js',
  'library/**/*.js',
] ;

let options = {
  // Specify style of output
  //'formatter': 'compact',
  // Only display warnings if a test is failing
  'alwaysWarn': true,
  // Increase the timeout of the test if linting takes to long
  'timeout': 5000
} ;

// Run the tests
lint(paths, options) ;

