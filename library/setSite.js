'use strict' ;

/**
 * library/setSite.js
 */

/**
 * Définit sur quel site on se trouve et retourne un objet permettant de 
 * personnaliser la vue.
 * Attention, à exécuter avant un `$view.setLayout()`, car la fonction
 * applique le layout par défaut associé à un site !
 * @param {View} $view Vue associée au controlleur
 * @param {String} [name='www'] Nom du site ou le domaine (considéré comme
 *    tel s'il contient au moins un `.` et dans ce cas, le dernier morceau
 *    sera supprimé (`www.theo-net.org` -> `www.theo-net`)
 * @returns {Object}
 */
function setSite ($view, name) {

  let site ;

  // `name` est un nom de domaine
  if (name) name = name.replace(/(.+)\..+$/g, '$1') ;

  switch (name) {

    case 'admin': 
    case 'admin.theo-net':  
      site = {
        name: 'admin',
        title: 'Admin',
        headTitle: 'Théo-Net',
        mainBckgrnd: 'darkGray',
        headerBckgrndColor: 'bittersweet-l',
        layout: __dirname + '/../controller/layoutAdmin.html'
      } ;
      break ;

    case 'kepha': 
    case 'kepha.theo-net':  
      site = {
        name: 'kepha',
        title: 'Kepha',
        headTitle: 'Théo-Net',
        mainBckgrnd: 'grass-l',
        headerBckgrndColor: 'mint',
        layout: __dirname + '/../controller/layoutKepha.html'
      } ;
      break ;

    case 'frat': 
    case 'frat.theo-net':  
    case 'www.fraternite-saint-etienne':  
      site = {
        name: 'frat',
        title: 'Fraternité Saint Étienne',
        headTitle: 'Fraternité Saint Étienne',
        mainBckgrnd: 'sunflower',
        headerBckgrndColor: 'sunflower',
        headerBckgrndImg: '/img/fondFrat.jpg',
        layout: __dirname + '/../controller/layoutFrat.html'
      } ;
      break ;

    default:
      site = {
        name: 'www',
        title: '<span class="headerSite__onlyBig">mission </span>Terre Sainte',
        headTitle: 'Théo-Net',
        mainBckgrnd: 'sunflower-l',
        headerBckgrndColor: 'mediumGray',
        headerBckgrndImg: '/img/fond.jpg'
      } ;
      break ;
  }

  if (site.layout) $view.$setLayout(site.layout) ;
  $view._site = site ;

  return site ;
}

module.exports = setSite ;

