'use strict' ;

/**
 * library/User.js
 */

const crypto = require('crypto') ;

const KjsUser = require('../kephajs/src/server/User/User'),
      newUuid = require('../kephajs').uuid ;

class User extends KjsUser {

  init () {

    this._connected = false ;

    // Création du hash du browser
    let hash = crypto.createHash('sha384') ;
    hash.update(this.browserInfo().signature) ;
    this._browserHash = hash.digest('base64') ;

    // L'utilisateur est-il connecté ?
    if (this.get('_connected'))
      this._connected = true ;
    // Sinon on regarde s'il a une connection persistante dispo
    else if (this.getCookie('tn_persist')) {

      // On parse le cookie (sécurité présente si le cookie a été corrompu)
      let tn_persist = (this.getCookie('tn_persist') + ':::a:::b').split(':::'),
          id         = tn_persist[0],
          token      = tn_persist[1] ;

      // Aller chercher dans connections.compare si le hash est bon
      return this.kernel().get('$models').get('connections')
        .compare(id, token, this._browserHash)
        .then(username => {
   
          return this.kernel().get('$models').get('users').getByCn(username)
          .then(user => {
            return user.getCircles().then(circles => {

              return this.kernel().get('$models').get('connections')
                .delete(id)
                .then(() => {
                  return this.setConnected(user, circles, true) ;
                }) ;
            }) ;
          }) ;
        })
        .catch(() => {
          return this.setDisconnected() ;
        }) ;
    }
  }

  isConnected () {

    return this._connected ; 
  }

  setConnected (user, circles, persist) {

    return new Promise((resolve, reject) => {

      this.reset() ;
      this.set('_connected', true) ;
      this.set('_user', user) ;
      this.set('_circles', circles) ;
      this._connected = true ;

      // La session sera multi-domaine
      this.set('cookieToken', newUuid()) ;
      this.kernel().get('$tmp')
          .set('sessionCookies.' + this.get('cookieToken'), this._sessionId) ;
   
      // Connexion persistante
      if (persist) {

        this.kernel().get('$models').get('connections')
          .store(this._browserHash, user.username)
          .then(cookie => { 

            this.setCookieMulti(
              'tn_persist', 
              cookie, 
              null, 60 * 60 * 24 * 30, 
              this._config.sessionCookiePath,
              this._config.sessionCookieSecure,
              this._config.sessionCookieHttpOnly
            ) ; 
            resolve() ;
          })
          .catch((e) => {

            console.error(e) ;
            reject(e) ;
          }) ;
      }
      else
        resolve() ;
    }) ;
  }

  setDisconnected () {

    let id = null ; 

    // On révoque la connexion persistante
    if (this.getCookie('tn_persist'))
      id = (this.getCookie('tn_persist') + ':::a:::b').split(':::')[0] ;

    this.reset() ;
    this.deleteCookieMulti(
      'tn_persist', 
      this._config.sessionCookiePath,
      this._config.sessionCookieSecure,
      this._config.sessionCookieHttpOnly
    ) ; 
    this._connected = false ;

    if (id) 
      return this.kernel().get('$models').get('connections').delete(id) ; 
    else
      return Promise.resolve() ;
  }

  connection (login, password, persist = false) {

    return this.kernel().get('ldap').testAuth(login + ',cn=users', password)
      .then(() => {
          
        return this.kernel().get('$models').get('users').getByCn(login)
        .then(user => {

          return user.getCircles().then(circles => {

            return this.setConnected(user, circles, persist) ;
          }) ;
        }) ;
      }) ;
  }

  isAdmin () {

    let admin = false ; 

    if (this.isConnected()) {

      this.get('_circles', []).forEach(circle => {

        if (circle.name == 'admin')
          admin = true ;
      }) ;
    }

    return admin ;
  }


  getUser () {

    return this.get('_user') ;
  }


  getCookieUrl (site) {
  
    let config = this.kernel().get('$config'), 
        url    = 'http',
        host   = this._request.headers.host.split(':') ;
    
    url += (config.get('forceHttps', false) ? 's' : '') + '://www.'
        + (site == 'frat' ? config.get('mainDomain') : config.get('fratDomain'))
        + (host.length > 1 ? ':' + host[1] : '')
        + '/cookie-' 
        + (this.has('cookieToken') ? this.get('cookieToken') 
          : this.kernel().get('$tmp').get('defaultCookieToken')) 
        + '.png' ;

    return url ;
  }


  /**
   * Créé un cookie multi-domaine. Mêmes arguments que la méthode `setCookie`
   * sauf `domain` qui est absent.
   * Pour cela, utilisera la liste `sessionCookieDomain` de la configuration,
   * de la même manière que pour définir un cookie de session : il prendra la
   * valeur définie lors de l'initialisation de l'objet. 
   * @param {String} name Nom du cookie
   * @param {String} value Valeur du cookie
   * @param {Integer} expires Date d'expiration (timestamp)
   * @param {Integer} maxAge Durée de vie du cookie
   * @param {String} path Chemin pour lesquels s'applique le cookie
   * @param {Boolean} secure Le cookie ne sera transmis qu'en HTTPS
   * @param {Boolean} httpOnly Le cookie ne sera disponible que dans les
   *        requête, pas dans la page à travers les méthodes `Document.cookie`,
   *        `XMLHttpRequest` ou `Request`
   */
  setCookieMulti (name, value, expires, maxAge, path, secure, httpOnly) {

    if (!this.has('cookieToken')) 
      this.set('cookieToken', newUuid()) ;
      
    let cookies = this.get('cookiesAdd', []) ;
    cookies.push({
      name, value, expires, maxAge, path, secure, httpOnly
    }) ;
    this.set('cookiesAdd', cookies) ;

    this.setCookie(
      name, value, 
      expires, maxAge, 
      this._sessionCookieDomain, 
      path, 
      secure, httpOnly
    ) ;
  }


  /**
   * Idem que `setCookieMulti`, mais pour effacer un cookie
   * @param {String} name Cookie à supprimer
   * @param {String} path Chemin pour lesquels s'applique le cookie
   * @param {Boolean} secure Le cookie ne sera transmis qu'en HTTPS
   * @param {Boolean} httpOnly Le cookie ne sera disponible que dans les
   *        requête, pas dans la page à travers les méthodes `Document.cookie`,
   *        `XMLHttpRequest` ou `Request`
   */
  deleteCookieMulti (name, path, secure, httpOnly) {

    if (!this.has('cookieToken')) 
      this.set('cookieToken', newUuid()) ;

    let cookies = this.get('cookiesDel', []) ;
    cookies.push({
      name, path, secure, httpOnly
    }) ;
    this.set('cookiesDel', cookies) ;

    this.deleteCookie(
      name, 
      this._sessionCookieDomain, 
      path, secure, httpOnly
    ) ;
  }
}

module.exports = User ; 

