'use strict' ;

/**
 * library/setSite.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const setSite = require('./setSite'),
      View    = require('../kephajs/src/server/View') ;

describe('library/setSite', function () {

  let view ; 

  beforeEach(function () {

    view = new View(null) ;
  }) ;

  it('retourne un objet, définit $view._site', function () {

    let site = setSite(view) ;

    expect(site).to.be.not.null ;
    expect(site).to.be.not.undefined ;
    expect(site).to.be.equal(view._site) ;
  }) ;

  it('la valeur par défaut est www', function () {

    expect(setSite(view, 'truc')).to.be.deep.equal(setSite(view)) ;
    expect(view._site.name).to.be.equal('www') ;
  }) ;

  it('site www', function () {

    setSite(view, 'www') ;
    expect(view._site.name).to.be.equal('www') ;
  }) ;

  it('site admin', function () {

    setSite(view, 'admin') ;
    expect(view._site.name).to.be.equal('admin') ;
  }) ;

  it('site admin.theo-net.* = admin', function () {

    setSite(view, 'admin.theo-net.dev') ;
    expect(view._site.name).to.be.equal('admin') ;
  }) ;

  it('site kepha', function () {

    setSite(view, 'kepha') ;
    expect(view._site.name).to.be.equal('kepha') ;
  }) ;

  it('site kepha.theo-net.* = kepha', function () {

    setSite(view, 'kepha.theo-net.dev') ;
    expect(view._site.name).to.be.equal('kepha') ;
  }) ;

  it('site frat', function () {

    setSite(view, 'frat') ;
    expect(view._site.name).to.be.equal('frat') ;
  }) ;

  it('site www.fraternite-saint-etienne.* = frat', function () {

    setSite(view, 'www.fraternite-saint-etienne.fr') ;
    expect(view._site.name).to.be.equal('frat') ;
  }) ;

  it('si pas de layout à définir, pas de $setLayout', function () {
  
    let spy = sinon.spy(view, '$setLayout') ;
    setSite(view) ;

    expect(spy.called).to.be.false ;
  }) ;

  it('sinon le définit', function () {

    let spy = sinon.spy(view, '$setLayout') ;
    setSite(view, 'admin') ;

    expect(spy.calledWith(__dirname + '/../controller/layoutAdmin.html'))
      .to.be.true ;
  }) ;
}) ;

