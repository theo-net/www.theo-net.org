'use strict' ;

/**
 * library/Mail.js
 */

const nodemailer = require('nodemailer') ;

const kjs = require('../kephajs') ;

/**
 * Gère l'envoit des mails
 */
class Mail {

  /**
   * Initialise le service et créé le canal d'events `mail`
   * @param {Object} config Configuration du service qui est un objet de la 
   *      forme suivante : 
   *        {                         // sera le nom du transporteur et l'email
   *          'toto@example.com': {   // de l'expéditeur 
   *            from: 'Le nom a affiché pour l\'expéditeur'
   *            host: '...'           // cf doc https://nodemailer.com/smtp/ 
   *            ...                   // pour la liste des autres options
   *          },
   *          'autre-mail@domaine.fr': {}, ...
   *        }
   * @param {Kernel} kernel Kernel de l'application
   */
  constructor (config, kernel) {

    this._kernel = kernel ;

    // Créé un event et attache les logs
    this._kernel.events().register('mail') ;
    this._kernel.events().attach('mail', 'log', data => {
      this._kernel.container().get('$logs').log(data.data, data.type) ;
    }) ;

    this._transporters = {} ;
    this._from = {} ;
    kjs.forEach(config, (options, id) => {
      
      if (options.from) {
        this._from[id] = options.from + '<' + id + '>' ;
        delete options.from ;
      }
      else
        this._from[id] = id ;

      this._transporters[id] = nodemailer.createTransport(options) ;
      this._kernel.events().notify(
        'mail',
        {type: 'info', data: 'Transporteur <' + id + '> initialisé'}
      ) ;
    }) ;
  }


  /**
   * Tests tous les transporteurs et renvoit, à travers une `Promise`,  un 
   * `Array` avec les résultats sous la forme : 
   *    [
   *      {
   *        id:  'truc@domaine.com',
   *        test: true                  // réussi à établir une connexion 
   *      }, {
   *        id:   'id_du@transporteur',
   *        test: {...},                // connexion impossible
   *      },
   *      ...
   *    ]
   * Lorsque la connexion est impossible, on retourne un objet contenant les
   * détail de l'erreur correspondante. 
   * @returns {Promise}
   */
  testTransporters () {

    let promises = [] ;

    kjs.forEach(this._transporters, (transporter, id) => {

      promises.push(new Promise(resolve => {
        transporter.verify(error => {
          if (error) resolve({id, test: error}) ;
          else resolve({id, test: true}) ;
        }) ;
      })) ;
    }) ;

    return Promise.all(promises) ;
  }


  /**
   * Envoit un ou plusieurs emails. Sauf si précisé dans les options, le 
   * champ `from` est construit à partir de ce qui a été utilisé pour 
   * configurer le service.
   * @param {String} id Id du transporteur (email)
   * @param {Array|Object} options Options du/des message(s) (dont contenu), 
   *        cf la doc https://nodemailer.com/message/ (On transmet un `Array`
   *        si on souhaite envoyer une série de mails)
   * @throws {Error} Si l'id ne correspond à rien
   * @returns {Promise}
   */
  sendMail (id, options) {

    if (!this._transporters[id])
      throw Error('Aucun transporteur <' + id + '> initialisé !') ;

    if (!Array.isArray(options))
      options = [options] ;

    let promises = [] ; 

    options.forEach(msg => {
      
      if (!msg.from)
        msg.from = this._from[id] ;

      promises.push(new Promise((resolve, reject) => {
      
        this._transporters[id].sendMail(msg, error => {

          this._kernel.events().notify(
            'mail',
            {type: 'data', data: '<' + id + '> send mail to ' + msg.to}
          ) ;

          if (error) reject(error) ;
          else resolve() ;
        }) ;
      })) ;
    }) ;

    return Promise.all(promises) ;
  }
}

module.exports = Mail ;

