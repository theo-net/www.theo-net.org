'use strict' ; 

/**
 * library/makePagination.js
 */


/**
 * Génère une pagination
 * @param {Object} pagination Description de la pagination : objet avec les
 *    méthodes suivantes :
 *     - `before` : lien vers page précédente
 *     - `after` : lien vers page suivante
 *     - `total` : nombre de pages
 *     - `current` : page courrante
 * @returns {String}
 */
function makePagination (pagination) {

  if (pagination.total < 2)
    return '' ; 

  pagination.current = Number(pagination.current) ;

  if (!pagination.first)
    pagination.first = _makeLink(pagination.link, 1) ;

  const max   = 15,
        marge = _setMarge(max) ;

  let out = '<nav class="mtm mbm txtcenter">'
    + '<ul class="pagination">' ;

  // Previous
  out += '<li class="'
      + (pagination.current != 1 ? 'active' : 'disabled')
      + '"><a href="'
      + (pagination.current > 2 ? 
        _makeLink(pagination.link, pagination.current - 1) : pagination.first)
      + '"><i class="icon icon-chevron-left"></i></a></li>' ;


  // Taille maximum de la pagination
  if (pagination.total <= max)
    out += _makePages(1, pagination.total, pagination) ;
  // Sinon on a une césure
  else {
  
    // Current au début
    if (pagination.current <= marge * 2 + 2) {

      out += _makePages(1, pagination.current + marge, pagination) ;
      out += '<li class="disabled"><a href=""><strong>…</strong></a></li>' ;
      out += _makePages(
        pagination.total - (max - (pagination.current + marge)) + 2,
        pagination.total, 
        pagination) ;
    }
    // Current à la fin
    else if (pagination.current >= pagination.total - (marge * 2 + 2)) {

      out += _makePages(
        1,
        max - (pagination.total - (pagination.current - (marge + 2))),
        pagination) ;
      out += '<li class="disabled"><a href=""><strong>…</strong></a></li>' ;
      out += _makePages(
        pagination.current - marge,
        pagination.total,
        pagination) ;
    }
    // Current au milieu
    else {
      out += _makePages(1, marge, pagination) ;
      out += '<li class="disabled"><a href=""><strong>…</strong></a></li>' ;
      out += _makePages(
        pagination.current - marge, 
        pagination.current + marge, 
        pagination) ;
      out += '<li class="disabled"><a href=""><strong>…</strong></a></li>' ;
      out += _makePages(
        pagination.total - marge + 1, 
        pagination.total, 
        pagination) ;
    }
  }


  // Next
  out += '<li class="'
      + (pagination.current != pagination.total ? 'active' : 'disabled')
      + '"><a href="' 
      + (pagination.current != pagination.total ? 
        _makeLink(pagination.link, pagination.current + 1) : '')
      + '"><i class="icon icon-chevron-right"></i></a></li>' ;

  out += '</ul></nav>' ;

  return out ; 
}


/**
 * En fonction d'une taille maximale de la pagination, détermine la marge
 * idéale.
 * @param {Integer} max Taille maximale de la pagiantion
 * @returns {Integer} Marge calculée
 * @private
 */
function _setMarge (max) {

  // Minimum trois éléments : le courant, les "..." avant et après
  // Minimum 4 fois la marge : début, fin, avant et après l'élément courant
  let marge = Math.floor((max - 3) / 4) ;

  return marge < 2 ? 1 : marge ;
}


/**
 * Construit une série de lien
 * @param {Integer} start Début
 * @param {Integer} end Fin
 * @param {Object} pagination Objet pagination
 * @returns {String}
 * @private
 */
function _makePages (start, end, pagination) {

  let out = '' ;
    
  for (let i = start ; i <= end ; i++) {

    out += '<li'
        + (pagination.current == i ? ' class="active"' : '') 
        + '><a href="'
        + (i == 1 ? pagination.first : _makeLink(pagination.link, i))
        + '">' + i + '</a></li>' ;
  }

  return out ;
}


/**
 * Construit un lien
 * @param {String} link Lien avec `{{num}}` qui sera remplacé par le numéro de 
 *    la page
 * @param {Integer} num Numéro de la page
 * @returns {String}
 * @private
 */
function _makeLink (link, num) {

  return link.replace('{{num}}', num) ;
}

module.exports = makePagination ;

