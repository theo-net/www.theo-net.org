'use strict' ;

/**
 * library/ldap.js
 */

const ldapjs = require('ldapjs') ;

const kephajs = require('../kephajs') ;

/**
 * Gère une conenction à un serveur LDAP
 */
class Ldap {

  /**
   * Connection à un serveur LDAP et créé le canal d'events `ldap`
   * @param {String} server Serveur Ldap `ldap://localhost:389`
   * @param {String} dn Base dn
   * @param {String} user Utilisateur
   * @param {String} password Mot de Passe
   */
  constructor (server, dn, user, password) {

    // Créé un event et attache les logs
    kephajs.kernel().events().register('ldap') ;
    kephajs.kernel().events().attach('ldap', 'log', data => {

      kephajs.kernel().container().get('$logs').log(data.data, data.type) ;
    }) ;

    this._client = ldapjs.createClient({
      url: server
    }) ;
    this._server = server ;
    this._dn = dn ;
      
    this._client.bind('cn=' + user + ',' + this._dn, password, () => {
    
      kephajs.kernel().events().notify(
        'ldap', 
        {type: 'info', data: 'Bind avec ' + user}
      ) ;
    }) ;

    kephajs.kernel().events().notify(
      'ldap', 
      {type: 'info', data: 'Connexion au server ' + server + ' (' + dn + ')'}
    ) ;
  }


  /**
   * Teste une authentification
   * @param {String} cn CN de l'utilisateur à tester
   * @param {String} password Mot de passe
   * @returns {Promise} Réussie si authentifié
   */
  testAuth (cn, password) {

    return new Promise((resolve, reject) => {
    
      let client = ldapjs.createClient({
        url: this._server
      }) ;
      client.bind('cn=' + cn + ',' + this._dn, password, err => {
    
        kephajs.kernel().events().notify(
          'ldap', 
          {type: 'data', data: 'Test d\'identification ' + cn}
        ) ;

        if (err) reject(err) ;
        else resolve() ;
      }) ;
    }) ;
  }


  /**
   * Retourne une série d'éléments
   * @param {String} filter Filtre à appliquer
   * @param {Array} attributes Liste des attributs à retourner
   * @param {Objet} [entity] Modèle de l'entité, si vaut `'raw'` retourne les
   *                         données brutes
   * @returns {Promise}
   */
  search (filter, attributes, entity) {

    return new Promise((resolve, reject) => {

      this._client.search(this._dn, {filter, scope: 'sub', attributes}, 
      (err, res) => {
    
        kephajs.kernel().events().notify(
          'ldap',
          {type: 'data', data: 'Search: ' + filter + ' in ' + this._dn}
        ) ;

        if (err) reject(err) ;
        
        let results = [] ;
        res.on('searchEntry', entry => {
          if (entity == 'raw')
            results.push(entry.raw) ;
          else if (entity)
            results.push(new entity(entry.object)) ;
          else
            results.push(entry.object) ;
        }) ;
        res.on('error', err => {
          kephajs.kernel().events().notify(
            'ldap',
            {type: 'error', data: err}
          ) ;
          reject(err) ;
        }) ;
        res.on('end', () => {
          resolve(results) ;
        }) ;
      }) ;
    }).catch(err => {
      
      kephajs.kernel().events().notify(
        'ldap',
        {type: 'error', data: err}
      ) ;
      Promise.reject(err) ;
    }) ; 
  }


  /**
   * Retourne un nouveau descripteur de changements
   * @param {Object} descriptor Le descripteur
   * @returns {ldap.Change}
   */
  change (descriptor) {

    return new ldapjs.Change(descriptor) ;
  }


  /**
   * Modifie un enreigstrement
   * @param {String} cn CN (sans le suffixe commun)
   * @param {Object|Array} change Changements
   * @returns {Promise}
   */
  modify (cn, change) {

    return new Promise((resolve, reject) => {

      this._client.modify(cn + ',' + this._dn, change, err => {
    
        kephajs.kernel().events().notify(
          'ldap',
          {type: 'data', data: 'Modify: ' + cn + ',' + this._dn}
        ) ;

        if (err) reject(err) ;
        else resolve() ;
      }) ;
    }).catch(err => {
      
      kephajs.kernel().events().notify(
        'ldap',
        {type: 'error', data: err}
      ) ;
      return Promise.reject(err) ;
    }) ; 
  }


  /**
   * Modifie le CN d'un enreigstrement
   * @param {String} cn Ancien CN (sans le suffixe commun)
   * @param {String} newCn Nouveau CN (sans le suffixe commun)
   * @returns {Promise}
   */
  modifyDN (cn, newCn) {

    return new Promise((resolve, reject) => {

      this._client.modifyDN(cn + ',' + this._dn, newCn + ',' + this._dn, 
      err => {
    
        kephajs.kernel().events().notify(
          'ldap', {
            type: 'data', 
            data: 'ModifyCN: ' + cn + ',' + this._dn + ' => ' + newCn + '...'
          }
        ) ;

        if (err) reject(err) ;
        else resolve() ;
      }) ;
    }).catch(err => {
      
      kephajs.kernel().events().notify(
        'ldap',
        {type: 'error', data: err}
      ) ;
      return Promise.reject(err) ;
    }) ; 
  }


  /**
   * Ajoute un enreigstrement
   * @param {String} dn DN à ajouter
   * @param {Object} entry Entrée à ajouter
   * @returns {Promise}
   */
  add (dn, entry) {

    return new Promise((resolve, reject) => {

      this._client.add(dn + ',' + this._dn, entry, err => {
    
        kephajs.kernel().events().notify(
          'ldap',
          {type: 'data', data: 'Add: ' + dn + ',' + this._dn}
        ) ;

        if (err) reject(err) ;
        else resolve() ;
      }) ;
    }).catch(err => {
      
      kephajs.kernel().events().notify(
        'ldap',
        {type: 'error', data: err}
      ) ;
      return Promise.reject(err) ;
    }) ; 
  }


  /**
   * Supprime une entrée dans le serveur LDAP
   * @param {String} dn DN à ajouter
   * @returns {Promise}
   */
  delete (dn) {

    return new Promise((resolve, reject) => {

      this._client.del(dn + ',' + this._dn, err => {
    
        kephajs.kernel().events().notify(
          'ldap',
          {type: 'data', data: 'Del: ' + dn + ',' + this._dn}
        ) ;

        if (err) reject(err) ;
        else resolve() ;
      }) ;
    }).catch(err => {
      
      kephajs.kernel().events().notify(
        'ldap',
        {type: 'error', data: err}
      ) ;
      return Promise.reject(err) ;
    }) ; 
  }


  /**
   * Échape les caractères spéciaux pour protéger les requêtes des injections
   * LDAP.
   * @param {String} type On échappe quoi (`filter` ou `dn`)
   * @param {String} value Chaîne à protéger
   * @returns {String}
   */
  escape (type, value) {

    if (type != 'filter' && type != 'dn')
      throw new Error('Ldap.protect type non reconnu') ;

    return value.replace(ESCAPES[type].regex, match => {
      return ESCAPES[type].replacements[match] ;
    }) ;
  }
}

const ESCAPES = {
  filter: {
    regex: new RegExp(/\0|\(|\)|\*|\\/g),
    replacements: {
      '\0': '\\00',
      '(': '\\28',
      ')': '\\29',
      '*': '\\2A',
      '\\': '\\5C'
    }
  },
  dn: {
    regex: new RegExp(/\0|"|#|\+|,|;|<|>|=|\\/g),
    replacements: {
      '\0': '\\00',
      ' ':  '\\ ',
      '"': '\\"',
      '#': '\\#',
      '+': '\\+',
      ',': '\\,',
      ';': '\\;',
      '<': '\\<',
      '>': '\\>',
      '=': '\\=',
      '\\': '\\5C'
    }
  }
} ;

module.exports = Ldap ;

