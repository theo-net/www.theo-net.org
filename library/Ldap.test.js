'use strict' ;

/**
 * library/Ldap.test.js
 */

const expect = require('chai').expect ;

const Ldap    = require('./Ldap'),
      kephajs = require('../kephajs') ;

kephajs.server() ;

describe('library/Ldap', function () {

  let ldap = new Ldap('ldap://localhost', '', '', '') ;

  describe('escape', function () {

    it('protection d\'un filter', function () {

      expect(ldap.escape('filter', '\0 (ok) * \\l'))
        .to.be.equal('\\00 \\28ok\\29 \\2A \\5Cl') ;
    }) ;

    it('protection d\'un dn', function () {

      expect(ldap.escape('dn', '\0 ("ok#)+,;<>=\\l'))
        .to.be.equal('\\00 (\\"ok\\#)\\+\\,\\;\\<\\>\\=\\5Cl') ;
    }) ;

    it('trhows error if type not good', function () {

      expect(() => { ldap.escape('foo', 'bar') ; })
        .to.throws(Error) ;
    }) ;
  }) ;
}) ;

