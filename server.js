'use strict' ;

const fs = require('fs') ;

const kephajs = require('./kephajs') ;
const server = kephajs.server(require('./config/config.json')) ;

const Ldap        = require('./library/Ldap'),
      Mysql       = require('./library/Mysql'),
      Mail        = require('./library/Mail'),
      Maintenance = require('./library/Maintenance') ;

// Liste des modèles
let Albums      = require('./entity/AlbumsRepository'),
    Articles    = require('./entity/ArticlesRepository'),
    Users       = require('./entity/UsersRepository'),
    Circles     = require('./entity/CirclesRepository'),
    Connections = require('./entity/ConnectionsRepository'),
    Categories  = require('./entity/CategoriesRepository') ;


// Init du server
server.init = ($config, kernel) => {

  // Maintenance
  kernel.container().set('maintenance', new Maintenance(
    __dirname + '/MAINTENANCE',
    kernel
  )) ;

  // Initialisation du répertoire de cache
  fs.mkdir(__dirname + '/tmp/cache', error => {
    if (error && error.code != 'EEXIST')
      throw error ;
  }) ;

  // LDAP
  kernel.container().set('ldap', new Ldap(
    $config.get('ldap').server,
    $config.get('ldap').dn,
    $config.get('ldap').user,
    $config.get('ldap').password
  )) ;

  // MySQL
  kernel.container().set('mysql', new Mysql(
    $config.get('mysql').host,
    $config.get('mysql').user,
    $config.get('mysql').password,
    $config.get('mysql').database,
    kernel
  )) ;

  // Mail
  kernel.container().set('mail', new Mail(
    $config.get('mail'),
    kernel
  )) ;

  // Helpers de View
  $config.set('$view', {
    helpers: {makePagination: require('./library/makePagination.js')}
  }) ;

  // On enregistre les modèles
  server.setModels({
    albums:   new Albums(),
    articles: new Articles(),
    articlesFrat: new Articles('Frat'),
    users:    new Users(kernel.get('ldap')),
    circles:  new Circles(kernel.get('ldap')),
    connections: new Connections(kernel.get('mysql')),
    categories:  new Categories(kernel.get('mysql'))
  }) ;

  // On enregistre les formulaires
  server.setFormBuilders({
    connection: require('./form/Connection'),
    user: require('./form/User'),
    circle: require('./form/Circle'),
    category: require('./form/Category'),
    contact: require('./form/Contact')
  }) ;
  // On personnalise leur rendu
  kernel.get('$forms').addClass({
    field: 'form-control',
    label: 'control-label',
    group: 'form-group has-feedback',
    groupSuccess: 'has-success',
    groupError: 'has-error',
    msg: 'feedback'
  }) ;

  // Objet personnalisé pour l'User
  server.kernel().get('$config')
    .set('$userObj', require('./library/User')) ;
  // Token du cookie par défaut (pour des cookies inter-domaines)
  kernel.get('$tmp').set('defaultCookieToken', kephajs.uuid()) ;

  // Cache des avatars
  kernel.get('$models').get('users').initCacheDir() ;

  // Un pseudo service permet de définir sur quel site l'utilisateur se trouve
  kernel.container().set('setSite', () => require('./library/setSite')) ;
} ;


// On lance le serveur
server.run() ;

