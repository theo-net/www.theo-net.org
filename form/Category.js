'use strict' ; 

const FormBuilder = require('../kephajs').FormBuilder ;

class CategoryFormBuilder extends FormBuilder {

  /**
   * @param {Object} params Objet avec les propriétés suivantes : 
   *    `categories` : la liste des catégories
   *    `circles` :    la liste des cercles
   *    `type` :        si égal à `add`, c'est un ajout, modification sinon
   */
  build (params) {

    let  config = {
      submit: 'Modifier',
      reset: 'Annuler les modifications'
    } ;
    this.getForm().type = 'edit' ;
 
    if (params && params.type == 'add') {
      config = {
        submit: 'Ajouter',
        reset: 'Remettre à zéro'
      } ;
      this.getForm().type = 'add' ;
    }

    // Liste des catégories
    let categories = [{value: '', label: '<em>Aucune</em>'}] ;
    if (params.categories && Array.isArray(params.categories)) {
      params.categories.forEach(category => {
        categories.push({value: category.id, label: category.name}) ;
      }) ;
    }
    // Liste des cercles
    let circles = [{value: '', label: '<em>Aucuns</em>'}] ;
    if (params.circles && Array.isArray(params.circles)) {
      params.circles.forEach(circle => {
        circles.push({value: circle.name, label: circle.toString()}) ;
      }) ;
    }
  
    this.getForm()
        .add('string', {
          name: 'name', 
          label: 'Nom de la catégorie',
          max: 255,
          required: true
        })
        .add('select', {
          name: 'parent', 
          label: 'Catégorie parente',
          list: categories
        })
        .add('select', {
          name: 'publishers', 
          label: 'Éditeurs',
          list: circles
        })
        .add('select', {
          name: 'authors', 
          label: 'Auteurs',
          list: circles
        })
      .getParent().addBlock('flex-item-center mtl')
      .add('submit', {
        name: 'submitBtn', 
        value: config.submit
      })
      .add('reset', {
        name: 'reset', 
        value: config.reset
      }) ;
  }
}

module.exports = CategoryFormBuilder ;

