'use strict' ; 

const FormBuilder = require('../kephajs').FormBuilder ;

class UserFormBuilder extends FormBuilder {

  /**
   * @param {String} type Si égal à `add`, c'est un ajout, modification sinon
   */
  build (type) {

    let  config = {
      newUsernameReq: false,
      newPasswordReq: false,
      submit: 'Modifier',
      reset: 'Annuler les modifications'
    } ;
    this.getForm().type = 'edit' ;
 
    if (type == 'add') {
      config = {
        newUsernameReq: true,
        newPasswordReq: true,
        submit: 'Ajouter',
        reset: 'Remettre à zéro'
      } ;
      this.getForm().type = 'add' ;
    }

    this.getForm()
      .addBlock('flex-container-h justify-space')
      .add('html', {name: 'blocAvatar'})
      .addFieldset('Identification')
        .add('string', {
          name: 'newUsername', 
          label: 'Nouveau nom d\'utilisateur',
          min: 3,
          required: config.newUsernameReq
        })
        .addBlock()
        .add('password', {
          name: 'newPassword', 
          label: 'Nouveau mot de passe',
          min: 8,
          help: '8 caractères minimums',
          required: config.newPasswordReq,
        })
        .add('password', {
          name: 'newPasswordCheck', 
          label: 'Retapez le mot de passe',
          required: config.newPasswordReq
        })
        .getParent()
        .add('email', {
          name: 'mail', 
          label: 'Adresse mail',
          required: true
        })
      .getParent().addFieldset('Informations personnelles')
        .add('string', {
          name: 'prenom', 
          label: 'Prénom',
          required: true
        }) 
        .add('string', {
          name: 'nom', 
          label: 'Nom',
          required: true
        })
      .getParent().addFieldset('Divers')
        .add('string', {
          name: 'description', 
          label: 'Nom à afficher'
        })
        .add('string', {
          name: 'quota', 
          label: 'Quota',
          required: true,
          help: 'De la forme «&nbsp;[Valeur] Mo&nbsp;» (ou Go)',
          pattern: /^\d+ [MG]o$/
        })
      .getParent().addBlock('flex-item-center ptl')
      .add('submit', {
        name: 'submitBtn', 
        value: config.submit
      })
      .add('reset', {
        name: 'reset', 
        value: config.reset
      }) ;
  }
}

module.exports = UserFormBuilder ;

