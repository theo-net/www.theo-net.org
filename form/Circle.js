'use strict' ; 

const FormBuilder = require('../kephajs').FormBuilder ;

class CirlceFormBuilder extends FormBuilder {

  /**
   * @param {String} type Si égal à `add`, c'est un ajout, modification sinon
   */
  build (type) {

    let  config = {
      newUsernameReq: false,
      newPasswordReq: false,
      submit: 'Modifier',
      reset: 'Annuler les modifications'
    } ;
    this.getForm().type = 'edit' ;
 
    if (type == 'add') {
      config = {
        newNameReq: true,
        submit: 'Ajouter',
        reset: 'Remettre à zéro'
      } ;
      this.getForm().type = 'add' ;
    }

    this.getForm()
        .add('string', {
          name: 'name', 
          label: 'Identifiant du cercle',
          min: 3,
          required: config.newNameReq
        })
        .add('string', {
          name: 'description', 
          label: 'Nom à afficher'
        })
      .getParent().addBlock('flex-item-center mtl')
      .add('submit', {
        name: 'submitBtn', 
        value: config.submit
      })
      .add('reset', {
        name: 'reset', 
        value: config.reset
      }) ;
  }
}

module.exports = CirlceFormBuilder ;

