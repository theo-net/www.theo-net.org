'use strict' ; 

const FormBuilder = require('../kephajs').FormBuilder ;

class ContactFormBuilder extends FormBuilder {

  build () {

    this.getForm()
        .add('string', {
          name: 'name', 
          label: 'Votre nom',
          max: 255,
          required: true
        })
        .add('email', {
          name: 'email', 
          label: 'Votre email',
          required: true
        })
        .add('string', {
          name: 'object', 
          label: 'Objet de votre demande',
          max: 255,
          required: true
        })
        .add('text', {
          name: 'message', 
          label: 'Votre messsage',
          rows: 10,
          required: true
        })
        .add('submit', {
          name: 'submitBtn', 
          value: 'Envoyer'
        }) ;
  }
}

module.exports = ContactFormBuilder ;
