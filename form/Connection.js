'use strict' ; 

const FormBuilder = require('../kephajs').FormBuilder ;

class ConnectionFormBuilder extends FormBuilder {

  build () {

    this.getForm()
      .resetClass()
      .addClass({
        field: 'form-control',
        label: 'control-label',
        group: 'form-group has-feedback',
        groupError: 'has-error',
        msg: 'feedback'
      })
      .addFieldset()
        .add('email', {
          name: 'mail', 
          label: 'E-mail',
          required: true,
          help: 'Votre e-mail est votre identifiant de connexion'
        })
        .add('password', {
          name: 'password', 
          label: 'Mot de passe',
          required: true,
          help: null
        })
        .add('checkbox', {
          name: 'persist',
          group: [
            {label: 'Se souvenir de moi', value: 'true'}
          ],
          class: {group: 'txtcenter mbm', field: 'form-control-toogle'}
        })
        .add('submit', {
          name: 'submitBtn', 
          value: 'Se connecter',
          class: {group: 'w50 center'}
        }) ;
  }
}

module.exports = ConnectionFormBuilder ;

