module.exports = {

// Configuration de ESlint
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
    'mocha': true
  },
  'parserOptions': {
    'ecmaVersion': 2017
  },

// Extension -> voir pour que tout soit importé d'un référenciel commun (KephaJs)
  'extends': 'eslint:recommended',

// Règles
  'rules': {

    // Erreurs possibles
    'no-console': 0,
    'valid-jsdoc': ['error', {
      'requireReturn': false,
      'requireReturnDescription': false
    }],
    //no-inner..?

    // Bonnes pratiques
    'block-scoped-var': 'error',
    'curly': ['error', 'multi-or-nest'],

    // Strict Mode
    'strict': ['error', 'global'],

    // Variables

    // Node.js

    // Styles
    'indent': ['error', 2, {
        'SwitchCase': 1,
        'VariableDeclarator': { 'var': 2, 'let': 2, 'const': 3 },
        'CallExpression': {
          'arguments': 'off'
        },
        'ignoredNodes': ['ConditionalExpression'],
        'MemberExpression': 'off'
      }
    ],
    'max-len': ['error', 80, 4,
      {'ignoreUrls': true, 'ignoreComments': true}
    ],
    'linebreak-style': ['error', 'unix'],
    'operator-linebreak': ['error', 'after', {
      'overrides': {'+': 'before', ':': 'before'}
    }],
    'quotes': ['error', 'single', {'avoidEscape': true}],
    'array-bracket-spacing': 'error',
    'no-spaced-func': 'error',
    'space-infix-ops': 'error',
    'space-in-parens': ['error', 'never'],
    'space-before-blocks': 'error',
    'block-spacing': 'error',
    'space-before-function-paren': [2, 'always'],
    'semi': ['error', 'always'],
    'semi-spacing': ['error', {'before': true, 'after': true}],
    'space-unary-ops': ['error', {
      'words': true,
      'nonwords': false,
    }],
    'key-spacing': ['error', {
      'afterColon': true,
      'mode': 'minimum'
    }],
    'one-var-declaration-per-line': ['error', 'initializations'],

    // ECMAScript 6
    'no-var': 'error',
  }
} ;

