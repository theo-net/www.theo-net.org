'use strict' ;

class PhotosController {

  indexAction () {

    this.get('setSite')(this.$view) ;
    this.$view.title = 'Les photos' ; 
    this.$view.photosActive = 'active' ; 

    this.$view.basecover = this.get('$config').get('albums').basecover ;
    this.$view.baseurl = this.get('$config').get('albums').baseurl ;

    this.$view.albums = this.get('$models').get('albums').getAll() ;
  }
}

module.exports = PhotosController ; 

