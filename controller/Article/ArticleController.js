'use strict' ;

const kephajs = require('../../kephajs') ;

const BaseAdmin = require('../BaseAdminController'),
      Category  = require('../../entity/Category') ;

class ArticleController extends BaseAdmin {

  listCategoriesAction () {

    this.$view.title = 'Liste des catégories' ; 
    
    return this.get('$models').get('categories').getAll().then(result => {
  
      this.$view.categories = result ; 
      
      return this.get('$models').get('categories').getAllTree().then(result => {
        console.log(result) ;
        this.$view.categoriesB = result ; 
      }) ;
    }) ;
  }


  editCategoryAction (vars) {

    let $models = this.get('$models') ;

    // On récupère la liste des catégories et des circles pour le formulaire
    return $models.get('categories').getAll().then(categories => {
      return $models.get('circles').getAll().then(circles => {
        return $models.get('categories').getById(vars.id).then(category => {

          if (category == undefined) 
            return Promise.reject(404) ;

          this.$view.title = 'Édition de la catégorie ' + category ; 
          let form = this.get('$forms').build(
            'category', category, {
              categories, circles, type: 'edit'
            }) ;
          this.$view.form = form ;

          form.setAction(
            this.$view.$getRoute('adminCategoryEdit', {id: category.id}), 
            'post'
          ) ;
          
          return this._categoryForm(form, category, vars) ;
        }) ;
      }) ;
    }) ;
  }


  addCategoryAction (vars) {

    this.$view.title = 'Ajout d’une categorie' ; 
    
    let category = new Category(vars, true) ;

    // On récupère la liste des catégories et des circles pour le formulaire
    return this.get('$models').get('categories').getAll().then(categories => {
      return this.get('$models').get('circles').getAll().then(circles => {
    
        let form = this.get('$forms').build(
          'category', category, {
            categories, circles, type: 'add'
          }) ;
        this.$view.form = form ;

        form.setAction(
          this.$view.$getRoute('adminCategoryAdd'), 
          'post'
        ) ;

        return this._categoryForm(form, category, vars) ;
      }) ;
    }) ;
  }


  deleteCategoryAction (vars) {

    this.$view.title = 'Suppression d’un ' 
                     + (vars.type == 'users' ? 'utilisateur' : 'cercle') ;
    
    return this.get('$models').get(vars.type).getById(vars.id).then(elmt => {

      if (!elmt) return Promise.reject(404) ;

      let token = this.$user.get('delete' + vars.type + vars.id) ;

      if (vars.token && vars.token == token) {

        return this.get('$models').get(vars.type).delete(elmt).then(() => {

          this.$user.setFlash(
            (vars.type == 'users' ? 'Utilisateur' : 'Cercle') 
            + ' définitivement supprimé&nbsp;!', 
            'success') ;
          this.$response.redirect(this.$view.$getRoute('adminUsers')) ;
        }).catch(() => {

          this.$user.setFlash(
            'Erreur lors de la suppression ' 
            + (vars.type == 'users' ? 'de l’utilisateur' : 'du cercle') 
            + '&nbsp;!', 
            'danger') ;
          this.$response.redirect(this.$view.$getRoute('adminUsers')) ;
        }) ;
      }
      else {
      
        this.$view.type = vars.type ;
        this.$view.id = vars.id ;
        this.$view.token = kephajs.uuid() ; 
        this.$user.set('delete' + vars.type + vars.id, this.$view.token) ;

        this.$view.msg = 
          (vars.type == 'users' ? 'l’utilisateur' : 'le cercle') 
          + ' «&nbsp;' + elmt + '&nbsp;»' ; 
      }
    }) ;
  }


  _categoryForm (form, category, vars) {

    // On a soumis le formulaire
    if (this.$request.method == 'POST') {

      if (!form.process(vars))
        this.$view.error = 'Certains champs ne sont pas valides' ;
      
      else {
 
        return this.get('$models').get('categories').save(category).then(() => {
                          
          if (form.type == 'add')
            this.$user.setFlash('Catégorie ajoutée', 'success') ;
          else {
            this.$user.setFlash(
              'Modification(s) enregistrée(s)', 'success'
            ) ;
          }
                          
          this.$response.redirect(this.$view.$getRoute('adminCategories')) ;
        }).catch((e) => {

          console.error(e) ; 
                            
          if (form.type == 'add')
            this.$view.error = 'Erreur lors de l’ajout' ;
          else 
            this.$view.error =  'Erreur lors de l’enregistrement' ;
        }) ;
      }
    }
  }
}

module.exports = ArticleController ;

