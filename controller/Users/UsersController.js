'use strict' ;

class UsersController {

  init () {

    this.get('setSite')(this.$view, this.$request.headers.host) ;
  }


  connectionAction (vars) {

    let form = this.get('$forms').build('connection') ;
    this.$view.title = 'Connexion' ;
    this.$view.form = form ;

    form.setAction(
      this.$view.$getRoute('connection'), 
      'post'
    ) ;

    if (this.$request.method == 'POST') {

      if (!form.process(vars))
        this.$view.error = 'Certains champs ne sont pas valides' ;

      else {

        return this.get('$models').get('users').getByMail(vars.mail)
        .then(user => {

          return this.$user
          .connection(user.username, vars.password, vars.persist) 
            .then(() => {
              this.$user.setFlash('Vous êtes bien connectés', 'success') ; 
              this.$response.redirect('./') ;
            })
            .catch((e) => {
              if (typeof e == 'string')
                this.$response.sendError500() ;
              else {
                form.getField('mail').setInvalid() ;
                form.getField('password').setInvalid() ;
                this.$view.error = 'Erreur d’authentification, '
                               + 'vos identifiants sont sans doute incorrects' ;
              }
            }) ;
        }).catch(() => {
          form.getField('mail')
            .setInvalid('Aucun utilisateur associé à cet e-mail') ;
          this.$view.error = 'Erreur d’authentification' ;
        }) ;
      }
    }
  }


  disconnectionAction () {

    return this.$user.setDisconnected().then(() => {

      this.$user.setFlash('Vous êtes bien déconnectés', 'success') ; 

      // si on se déconnecte depuis l'administration, on retourne sur www
      if (this.$view._site.name == 'admin')
        this.$view._site.name = 'www' ;

      this.$response.redirect(
        this.get('$router').unroute(
          this.$view._site.name + 'Index', {}, this.$request.headers.host
        )
      ) ;
    }) ;
  }


  accountAction () {

    this.$view.title = 'Mon compte' ;
  }


  avatarAction (vars) {

    let username = vars.username ;

    return this.get('$models').get('users').getByCn(username).then(user => {

      username = user ? user.username : 'nan' ;
     
      if (this.get('$cache').has('users.avatar.' + username)) {
        return Promise.resolve(
                  this.get('$cache').get('users.avatar.' + username)) ;
      }
      else {

        return this.get('$models').get('users').loadAvatar(username).then(
        file => {
          
          this.get('$cache').set('users.avatar.' + username, file) ;
          return Promise.resolve(file) ;
        }) ; 
      }
    }).then(file => { 
      
      return new Promise(resolve => {

        if (file === null) {
          resolve(this.$response.sendSlot('@static', null, 
            {file: 'img/avatar.jpg'})) ;
        }
        else {
          resolve(this.$response.sendSlot('@static', null, 
            {file: file, staticDir: '/'})) ;
        } 
      }) ;
    }) ;
  }


  cookieAction (vars) {
 
    let $tmp          = this.get('$tmp'),
        sessionCookie = 'sessionCookies.' + vars.token ;

    // On supprime 'cookieToken' avant tout switch de session
    this.$user.delete('cookieToken') ;
    
    if ($tmp.has(sessionCookie)) {

      this.$user.switchSession($tmp.get(sessionCookie)) ;
      $tmp.delete(sessionCookie) ;
    }

    // cookies multi-domaines
    this.$user.get('cookiesAdd', []).forEach(cookie => {
      this.$user.setCookie(
        cookie.name, cookie.value, 
        cookie.expires, cookie.maxAge,
        this.$user._sessionCookieDomain, cookie.path,
        cookie.secure, cookie.httpOnly
      ) ;
    }) ;
    this.$user.delete('cookiesAdd') ;
    this.$user.get('cookiesDel', []).forEach(cookie => {
      this.$user.deleteCookie(
        cookie.name, 
        this.$user._sessionCookieDomain, cookie.path,
        cookie.secure, cookie.httpOnly
      ) ;
    }) ;
    this.$user.delete('cookiesDel') ;

    // On retourne un pixel
    return this.$response.sendSlot('@static', null, {file: 'img/pixel.png'}) ;
  }
}

UsersController.prototype.setViews = {
  disconnection: null,
  avatar: null,
  cookie: null
} ;

module.exports = UsersController ;

