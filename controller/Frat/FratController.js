'use strict' ;

const kjs = require('../../kephajs') ;

class FratController {

  init () {

    this.get('setSite')(this.$view, 'frat') ;
  }


  charteAction () {

    this.$view.title = 'Charte de vie' ; 
    this.$view.charteActive = 'active' ;
  }


  aProposAction (vars) {

    this.$view.title = 'À propos' ; 
    this.$view.aProposActive = 'active' ;

    let form = this.get('$forms').build('contact') ;
    this.$view.form = form ;
    form.setAction(
      this.$view.$getRoute('fratAPropos'), 
      'post'
    ) ;

    if (this.$request.method == 'POST') {

      if (!form.process(vars))
        this.$view.error = 'Certains champs ne sont pas valides' ;
      else if (this._hasFlood(this.$request)) {
        this.$view.error = 'Vous devez patientez 30&nbsp;s '
                         + 'entre chaque soumission du formulaire' ;
      }
      else {
        
        let subject = '[Fraternité Saint Étienne] Demande de contact' ;
        let messages  = [
          {
            to: 'contact@fraternite-saint-etienne.fr',
            replyTo: vars.email,
            subject: subject + ' : ' + vars.object,
            text: 'Bonjour, vous avez reçu une nouvelle demande de contact. '
                + 'En répondant à ce message, vous enverez un mail directement '
                + 'à : \n\t' + vars.name + ' <' + vars.email + '>\n\n'
                + 'Objet : ' + vars.object + '\n\nMessage : \n' + vars.message
          }, {
            to: vars.email,
            subject: subject,
            text: 'Bonjour ' + vars.name + ', votre message a bien été pris en '
                + 'compte. Nous y répondrons dès que possible. \n\nVoici une '
                + 'copie de votre message : \n' + vars.message
                + '\n\n--- \nCeci est un mail automatique : ne pas y répondre.'
          }
        ] ; 

        return this.get('mail').sendMail(
          'no-reply@fraternite-saint-etienne.fr', 
          messages
        ).then(() => {
          this.$user.setFlash(
            'Votre message a bien été envoyé. Vous allez recevoir dans '
            + 'votre boîte mail une copie et nous vous répondrons dès que '
            + 'possible.', 'success'
          ) ;
        }).catch(error => {
          console.error(error) ;
          this.$view.error = 'Suite à une erreur interne, le message '
                           + 'n\'a pas pu être envoyé&nbsp;!' ;
        }) ;
      }
    }
  }


  /**
   * Permet de vérifier s'il y a du flood ou pas. On nettoie régulièrement
   * la mémoire. L'intervalle entre deux envois autorisés est de 30 s.
   * @param {http.ServerRequest} request La requête
   * @returns {Boolean}
   * @private
   */
  _hasFlood (request) {

    let flood    = this.get('$tmp').get('contactFlood', {}) ;

    kjs.forEach(flood, (timestamp, ip) => {
      console.log('ip ' + ip + ', timestamp ' + timestamp) ;
      if (Date.now() - timestamp > 30000)
        delete flood[ip] ;
    }) ;
    let hasFlood = !flood[request.connection.remoteAddress] ;
    
    flood[request.connection.remoteAddress] = Date.now() ;
    this.get('$tmp').set('contactFlood', flood) ;

    return !hasFlood ;
  }
}
module.exports = FratController ;

