'use strict' ;

const querystring = require('querystring') ; 

class MaintenanceController {

  indexAction () {

    this.$view.$setLayout(__dirname + '/../layoutEmpty.html') ;
    this.$view.title = 'Site en maintenance' ;
    this.$view.message = this.get('maintenance').getMessage() ;
    this.$response.setStatusCode(503) ;
  }

  setAction (vars) {

    this.$view.$setMimeType('json') ;

    if (this.get('$config').get('maintenanceToken') != vars.token) {

      this.$response.setStatusCode(403) ;
      this.$response.done('unauthorized') ;
    }
    else {

      try {

        if (vars.active == 'on') {
          this.get('maintenance').activate(
            querystring.unescape(vars.message.substr(1))
          ) ;
        }
        else
          this.get('maintenance').desactivate() ;

        let active = this.get('maintenance').isActive() ? 'on' : 'off' ;
        this.$response.done('{maintenance: ' + active + '}') ;
      }
      catch (e) {

        this.$response.setStatusCode(500) ;
        this.$response.done('error') ;
        return Promise.reject(e) ;
      }
    }
  }
}

MaintenanceController.prototype.setViews = {
  set: null
} ;

module.exports = MaintenanceController ;

