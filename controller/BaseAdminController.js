'use strict' ; 

class BaseAdminController {

  init () {

    this.$view.menu = [
      {url: '/index.html', desc: 'Administration'},
      {url: './users.html', desc: 'Utilisateurs'},
      {url: './services.html', desc: 'Services'},
      {url: './cache.html', desc: 'Cache'},
      {url: './categories.html', desc: 'Catégories'},
      {url: './articles.html', desc: 'Articles'}
    ] ;

    if (!this.$user.isAdmin())
      return Promise.reject(403) ;

    this.get('setSite')(this.$view, 'admin') ;

    this.$view._site.wide = true ;
  }
}

module.exports = BaseAdminController ; 

