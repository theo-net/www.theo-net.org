'use strict' ;

class NewsletterController {

  indexAction () {

    this.get('setSite')(this.$view) ;
    this.$view.title = 'La newsletter' ; 
    this.$view.newsletterActive = 'active' ; 
  }
}

module.exports = NewsletterController ;

