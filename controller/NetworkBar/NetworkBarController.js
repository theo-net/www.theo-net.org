'use strict' ;

class NetworkBarController {

  headerAction () {

    this.get('setSite')(this.$view, this.$request.headers.host) ;
    this.$view.$setLayout(false) ;

    this.$view.isConnected = this.$user.isConnected() ;

    if (this.$view.isConnected) {
      this.$view.networkUser = this.$user.getUser().toString() ;
      this.$view.networkUsername = this.$user.getUser().username ;
      this.$view.hasAdmin = this.$user.isAdmin() ;
    }

    let mainDomain = this.get('$config').get('mainDomain'),
        fratDomain = this.get('$config').get('fratDomain'),
        https      = 'http' 
                   + (this.get('$config').get('forceHttps') ? 's' : '') 
                   + '://' ;
    let port = this.$request.headers.host.match(/:\d+$/) ;
    port = port !== null ? port[0] : '' ; 

    this.$view.menuNetwork = [
      {url: https + 'www.' + mainDomain + port, desc: 'Accueil', icon: 'home'},
      {url: https + 'cloud.' + mainDomain, desc: 'Le cloud', icon: 'cloud'},
      {url: https + 'kepha.' + mainDomain + port, desc: 'Kepha', icon: 'empty'},
      {url: https + 'www.' + fratDomain + port, 
        desc: 'Fraternité Saint Étienne', icon: 'logo-frat-img'}
    ] ;

    this.$view.https = https ;
    this.$view.mainDomain = mainDomain ;
    this.$view.port = port ;
  }
}

module.exports = NetworkBarController ; 

