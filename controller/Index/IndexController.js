'use strict' ;

class IndexController {

  init () {

    this.get('setSite')(this.$view, this.$request.headers.host) ;
  }

  indexAction () {

    this.$view.title = 'Accueil' ;
    this.$view._site.big = true ;
    this.$view.accueilActive = 'active' ;

    this._articles() ; 

    // Message
    this.$view.message = require('../../data/accueilMsg') ;
  }

  pageAction (vars) {

    this.$view.title = 'Articles, page ' + vars.page ;
    
    return this._articles(vars.page) ; 
  }


  aproposAction () {

    this.$view.title = 'À propos' ; 
    this.$view.aproposActive = 'active' ;
  }


  licenceAction () {

    let start   = 2017,
        current = new Date() ;
    current = current.getFullYear() ;

    this.get('setSite')(this.$view, this.$request.headers.host.split('.')[0]) ;
    this.$view.title = 'MIT License' ; 
    this.$view.years = start + (current != start ? '-' + current : '') ; 
  }


  legalsAction () {

    this.get('setSite')(this.$view, this.$request.headers.host.split('.')[0]) ;
    this.$view.title = 'Mentions légales' ; 
  }


  _articles (page = 1) {

    let nbpp = this.get('$config').get(
      'articles' + (this.$view._site.name == 'frat' ? 'Frat' : ''), {nbpp: 1}
    ).nbpp ;

    // Articles
    const articlesRepository = this.get('$models').get(
      'articles' + (this.$view._site.name == 'frat' ? 'Frat' : '')
    ) ;

    let nbArticles = articlesRepository.count() ;

    // Pagination
    this.$view.pagination = {
      first: 'index.html',
      link: 'page-{{num}}.html',
      total: Math.ceil(nbArticles / nbpp),
      current: page
    } ;

    if (page > this.$view.pagination.total)
      return Promise.reject(404) ;

    this.$view.articles = articlesRepository
      .getList(
        nbArticles + 1 - page * nbpp, 
        (page * nbpp > nbArticles ? (nbArticles - nbpp * (page - 1)) : nbpp)
      ).reverse() ;
  }
}

module.exports = IndexController ;

