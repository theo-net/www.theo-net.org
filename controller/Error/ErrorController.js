'use strict' ;

class ErrorController {

  init () {

    this.get('setSite')(this.$view, this.$request.headers.host) ;
  }

  e403Action () {

    this.$view.title = 'Erreur d\'identification... ' ;
  }

  e404Action () {

    this.$view.title = 'Page non trouvée... ' ;
  }

  e500Action () {

    this.$view.title = 'Erreur interne... ' ;
  }
}

module.exports = ErrorController ;

