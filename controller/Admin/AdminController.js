'use strict' ;

const exec = require('child_process').exec ;

const kephajs = require('../../kephajs') ;

const BaseAdmin = require('../BaseAdminController'),
      User      = require('../../entity/User'),
      Circle    = require('../../entity/Circle') ;

class AdminController extends BaseAdmin {

  indexAction () {

    this.$view.title = 'Administration' ;

    let promises = [] ; 

    // Mails
    promises.push(this.get('mail').testTransporters().then(results => {

      this.$view.mails = results.map(res => {
              
        let out = '<span class="btn btn-' 
                + (res.test === true ? 'success' : 'danger')
                + '"><i class="icon icon-'
                + (res.test === true ? 'check' : 'close')
                + '"></i></span>' ;
          
        if (res.test !== true) {

          out += ' ' + '<code>' + res.test.code + '</code><br>' 
              + res.test + (res.test.reason ? 
                '<br><em>' + res.test.reason + '</em>' : '') ; 
        }

        return {id: res.id, out} ;
      }) ;
    })) ;

    // Users
    promises.push(this.get('$models').get('users').count().then(nb => {
      this.$view.nbUsers = nb ; 
    })) ;
    promises.push(this.get('$models').get('circles').count().then(nb => {
      this.$view.nbCircles = nb ; 
    })) ;
    promises.push(this.get('$models').get('connections').count().then(nb => {
      this.$view.nbConnections = nb ;
    })) ;
    this.$view.nbSessions = this.$user._sessions._storage.count() ;

    // Server info
    this.$view.server = {} ;
    this.$view.server.port = this.get('$config').get('port') ;
    promises.push(
      new Promise((success, reject) => {
        exec('lsb_release -s -d', (err, stdout) => {
          if (err)
            reject(err) ;
          else {
            this.$view.server.lsb = stdout ;
            success() ;
          }
        }) ;
      })
    ) ;

    // Memory usage
    this.$view.memory = process.memoryUsage() ;
    this.$view.memory.rss = Math.round(
      this.$view.memory.rss / 1024 / 1024 * 1000
    ) / 1000 ;
    this.$view.memory.heapUsed = Math.round(
      this.$view.memory.heapUsed / 1024 / 1024 * 1000
    ) / 1000 ;
    this.$view.memory.external = Math.round(
      this.$view.memory.external / 1024 / 1024 * 1000
    ) / 1000 ;

    return Promise.all(promises) ;
  }


  listUsersAction () {

    
    this.$view.title = 'Liste des utilisateurs' ; 
    
    return this.get('$models').get('users').getAll().then(result => {
  
      this.$view.users = result ; 
    
      return this.get('$models').get('circles').getAll().then(result => {
        
        this.$view.circles = [] ; 

        result.forEach(circle => {

          // Le cercle `users` n'a pas à être modifié (contient tous les autres
          // cercles et utilisateurs
          if (circle.name != 'users')
            this.$view.circles.push(circle) ;
        }) ;
      }) ;
    }) ;
  }


  editUserAction (vars) {

    return this.get('$models').get('users').getById(vars.id).then(user => {

      if (user == undefined) 
        return Promise.reject(404) ;

      let form = this.get('$forms').build('user', user) ;
      this.$view.title = 'Édition de l’utilisateur ' + user ; 
      this.$view.form = form ;
      this.$view.user = user ;

      form.setAction(
        this.$view.$getRoute('adminUserEdit', {id: user.id}), 
        'post'
      ) ;
   
      return user.getCircles().then(circles => {
       
        this.$view.circles = circles ;
        if (circles.length == 0) this.$view.emptyCircles = true ;

        return this._userForm(form, user, vars) ;
      }) ;
    }) ;
  }


  addUserAction (vars) {

    this.$view.title = 'Ajout d’un utilisateur' ; 
    
    let user = new User(vars, true) ;

    let form = this.get('$forms').build('user', user, 'add') ;
    this.$view.form = form ;

    form.setAction(
      this.$view.$getRoute('adminUserAdd'), 
      'post'
    ) ;

    return this._userForm(form, user, vars) ;
  }


  editCircleAction (vars) {

    return this.get('$models').get('circles').getById(vars.id).then(circle => {

      if (circle == undefined) 
        return Promise.reject(404) ;

      return this.get('$models').get('users').getAll().then(result => {
      
        let members = Array.isArray(circle.members) ? 
                      circle.members : [circle.members] ;

        this.$view.members = [] ;

        result.forEach(user => {
          
          let member = members.findIndex(element => {
            return element == user.username ;
          }) > -1 ;
          
          this.$view.members.push({
            user: user.toString(), member,
            id: user.id, action: (member ? 'remove' : 'add')
          }) ;
        }) ;

        let form = this.get('$forms').build('circle', circle) ;
        this.$view.title = 'Édition du cercle ' + circle ; 
        this.$view.gid = circle.id ;
        this.$view.form = form ;

        form.setAction(
          this.$view.$getRoute('adminCircleEdit', {id: circle.id}), 
          'post'
        ) ;
      
        return this._circleForm(form, circle, vars) ;
      }) ;
    }) ;
  }


  addCircleAction (vars) {

    this.$view.title = 'Ajout d’un cercle' ; 
    
    let circle = new Circle(vars, true) ;

    let form = this.get('$forms').build('circle', circle, 'add') ;
    this.$view.form = form ;

    form.setAction(
      this.$view.$getRoute('adminCircleAdd'), 
      'post'
    ) ;

    return this._circleForm(form, circle, vars) ;
  }


  deleteUserAction (vars) {

    this.$view.title = 'Suppression d’un ' 
                     + (vars.type == 'users' ? 'utilisateur' : 'cercle') ;
    
    return this.get('$models').get(vars.type).getById(vars.id).then(elmt => {

      if (!elmt) return Promise.reject(404) ;

      let token = this.$user.get('delete' + vars.type + vars.id) ;

      if (vars.token && vars.token == token) {

        return this.get('$models').get(vars.type).delete(elmt).then(() => {

          this.$user.setFlash(
            (vars.type == 'users' ? 'Utilisateur' : 'Cercle') 
            + ' définitivement supprimé&nbsp;!', 
            'success') ;
          this.$response.redirect(this.$view.$getRoute('adminUsers')) ;
        }).catch(() => {

          this.$user.setFlash(
            'Erreur lors de la suppression ' 
            + (vars.type == 'users' ? 'de l’utilisateur' : 'du cercle') 
            + '&nbsp;!', 
            'danger') ;
          this.$response.redirect(this.$view.$getRoute('adminUsers')) ;
        }) ;
      }
      else {
      
        this.$view.type = vars.type ;
        this.$view.id = vars.id ;
        this.$view.token = kephajs.uuid() ; 
        this.$user.set('delete' + vars.type + vars.id, this.$view.token) ;

        this.$view.msg = 
          (vars.type == 'users' ? 'l’utilisateur' : 'le cercle') 
          + ' «&nbsp;' + elmt + '&nbsp;»' ; 
      }
    }) ;
  }


  circleUserAction (vars) {

    return this.get('$models').get('users').getById(vars.id).then(user => {

      if (!user)
        return Promise.reject(404) ;

      return this.get('$models').get('circles').getById(vars.gid)
      .then(circle => {

        if (!circle)
          return Promise.reject(404) ;

        circle[vars.action + 'Member'](user.username) ;

        return this.get('$models').get('circles').save(circle).then(() => {
          
          this.$user.setFlash(
            'Utilisateur bien ' 
            + (vars.action == 'add' ? 'ajouté' : 'supprimé')
            + ' du cercle', 
            'success'
          ) ;
          this.$response.redirect(
            this.$view.$getRoute('adminCircleEdit', {id: vars.gid})
          ) ;
        }).catch(() => {
          
          this.$user.setFlash(
            'Erreur lors de ' 
            + (vars.action == 'add' ? 'l’ajout' : 'la suppression')
            + ' de l’utilisateur du cercle', 
            'danger'
          ) ;
          this.$response.redirect(
            this.$view.$getRoute('adminCircleEdit', {id: vars.gid})
          ) ;
        }) ;
      }) ;
    }) ;
  }


  servicesAction () {

    this.$view.title = 'Services' ;

    this.$view.services = [] ;
    Object.keys(this.kernel().container().getAll()).forEach(name => {
      this.$view.services.push({name}) ;
    }) ;

    // $config
    this.$view.configStr = [] ;
    this.$view.configObj = [] ;
    kephajs.forEach(this.get('$config').getAll(), (value, name) => {
      if (!kephajs.isObject(value))
        this.$view.configStr.push({name, value: value.toString()}) ;
      else
        this.$view.configObj.push({name}) ;
    }) ;

    // $validate
    this.$view.validators = [] ;
    kephajs.forEach(this.get('$validate')._validators, (val, name) => {
      this.$view.validators.push({name}) ;
    }) ;

    // $filter
    this.$view.filters = [] ;
    kephajs.forEach(this.get('$filter')._filters, (val, name) => {
      this.$view.filters.push({name}) ;
    }) ;

    // $events
    this.$view.events = [] ;
    kephajs.forEach(this.get('$events')._events, (val, name) => {
      this.$view.events.push({name}) ;
    }) ;
    
    // $color
    this.$view.styles = [] ;
    kephajs.forEach(this.get('$color')._styles, (val, name) => {
      this.$view.styles.push({name}) ;
    }) ;
    
    // $models
    this.$view.models = [] ;
    kephajs.forEach(this.get('$models')._models, (val, name) => {
      this.$view.models.push({name}) ;
    }) ;
    
    // $forms
    this.$view.formBuilders = [] ;
    this.$view.fields = [] ;
    kephajs.forEach(this.get('$forms')._formBuilders, (val, name) => {
      this.$view.formBuilders.push({name}) ;
    }) ;
    kephajs.forEach(this.get('$forms')._fields, (val, name) => {
      this.$view.fields.push({name}) ;
    }) ;
    this.$view.formClassField = [] ;
    this.get('$forms')._class.field.forEach(name => {
      this.$view.formClassField.push({name}) ;
    }) ;
    this.$view.formClassLabel = [] ;
    this.get('$forms')._class.label.forEach(name => {
      this.$view.formClassLabel.push({name}) ;
    }) ;
    this.$view.formClassGroup = [] ;
    this.get('$forms')._class.group.forEach(name => {
      this.$view.formClassGroup.push({name}) ;
    }) ;
    this.$view.formClassGroupSuccess = [] ;
    this.get('$forms')._class.groupSuccess.forEach(name => {
      this.$view.formClassGroupSuccess.push({name}) ;
    }) ;
    this.$view.formClassGroupError = [] ;
    this.get('$forms')._class.groupError.forEach(name => {
      this.$view.formClassGroupError.push({name}) ;
    }) ;
    this.$view.formClassMsg = [] ;
    this.get('$forms')._class.msg.forEach(name => {
      this.$view.formClassMsg.push({name}) ;
    }) ;
    this.$view.formClassMsgHelp = [] ;
    this.get('$forms')._class.msgHelp.forEach(name => {
      this.$view.formClassMsgHelp.push({name}) ;
    }) ;
    this.$view.formClassMsgError = [] ;
    this.get('$forms')._class.msgError.forEach(name => {
      this.$view.formClassMsgError.push({name}) ;
    }) ;
    this.$view.formClassFieldset = [] ;
    this.get('$forms')._class.fieldset.forEach(name => {
      this.$view.formClassFieldset.push({name}) ;
    }) ;
    
    // $router
    this.$view.routes = [] ;
    this.$view.routerFiles = [] ;
    kephajs.forEach(this.get('$router')._routes, (val, name) => {
      this.$view.routes.push({name}) ;
    }) ;
    this.get('$router').getFiles().forEach(file => {
      this.$view.routerFiles.push({path: file.file}) ;
    }) ;
    
    // ldap
    this.$view.ldap = {
      server: this.get('ldap')._server,
      dn: this.get('ldap')._dn
    } ;
    
    // mysql
    let mysql = this.get('mysql') ;
    this.$view.mysql = {
      user: mysql._config.user,
      host: mysql._config.host,
      port: mysql._connection.config.port,
      database: mysql._config.database
    } ;
  }


  cacheAction (vars) {

    this.$view.title = 'Cache' ;

    // Activation/Désactivation du cache
    this.$view.cacheEnabled = this.get('$cache').isEnabled() ;
    if (vars.toogle) {

      this.get('$cache').enable(!this.$view.cacheEnabled) ;
      return this.$response.redirect(this.$view.$getRoute('adminCache')) ;
    }


    // Reset des caches
    if (vars.reset == 'staticFiles') {
      this.get('$staticCache').reset() ;
      return this.$response.redirect(this.$view.$getRoute('adminCache')) ;
    }
    else if (vars.reset == 'avatars') {
      this.get('$models').get('users').initCacheDir() ; 
      this.get('$cache').delete('users.avatar.*') ;
      return this.$response.redirect(this.$view.$getRoute('adminCache')) ;
    }
    
    
    // Liste des staticFiles
    this.$view.staticFiles = [] ;
    Object.getOwnPropertyNames(this.get('$staticCache').getAll())
      .forEach(src => {
        this.$view.staticFiles.push({
          src: src.replace(this.get('$config').get('basepath'), '~/')
        }) ;
      }) ;
    if (this.$view.staticFiles.length == 0)
      this.$view.staticFilesEmpty = true ;

    // Liste des avatars
    this.$view.avatars = [] ;
    Object.getOwnPropertyNames(this.get('$cache').get('users.avatar.*'))
      .forEach(username => {
        this.$view.avatars.push({username}) ;
      }) ;
    if (this.$view.avatars.length == 0)
      this.$view.avatarsEmpty = true ;
  }


  /**
   * Test si le nom de l'utilisateur ou du cercle (`username` ou `name`) est
   * déjà pris ou non. 
   * @param {String} name Nom à tester
   * @returns {Promise} Avec `true` si déjà pris ou `false` sinon. Si renvoit
   *                    un entier, il s'agira du cercle avec le même nom
   */
  _nameExist (name) {

    return this.get('$models').get('users').getByCn(name)
    .then(result => {
      
      if (result && result.lenght != 0)
        return Promise.resolve(true) ;
        
      return this.get('$models').get('circles').getByCn(name)
      .then(result => {

        if (result && result.lenght != 0)
          return Promise.resolve(result.id) ;
          
        return Promise.resolve(false) ;
      }) ;
    }) ;
  }


  _userForm (form, user, vars) {

    if (form.type != 'add') {
      form.getField('blocAvatar')._html = '<p class="ptl">'
        + '<img src="/img/avatar-' + user.username 
        + '.jpg" class="avatarUserBig" alt="avatar"></p>' ;
    }

    let errorMsg = (form.type == 'add' ? 'Ajout' : 'Mise-à-jour') 
                 + ' impossible de l’utilisateur' ;

    // On a soumis le formulaire
    if (this.$request.method == 'POST') {

      if (!form.process(vars))
        this.$view.error = 'Certains champs ne sont pas valides' ;
      
      else if (vars.newPassword && vars.newPassword != vars.newPasswordCheck) {
        form.getField('newPasswordCheck')
            .setInvalid('Les mots de passes ne correspondent pas') ;
        form.getField('newPassword')
            .setInvalid('Les mots de passes ne correspondent pas') ;
        this.$view.error = errorMsg ;
      }
      else {
  
        let testUsername = user.username ; 
        if (vars.newUsername) {
          user.setUsername(vars.newUsername) ; 
          testUsername = vars.newUsername ; 
        }
     
        // Unicité mail
        return this.get('$models').get('users').getByMail(vars.mail)
        .then(result => {

          if (result && result.lenght != 0 && result.id != user.id) {
            form.getField('mail')
              .setInvalid('Un compte possède déjà cette adresse mail') ;
            this.$view.error = errorMsg ;
          }
          // Unicité id
          else {
            return this._nameExist(testUsername).then(exist => {

              if (vars.newUsername && exist) {
                form.getField('newUsername')
                    .setInvalid('Le nom d’utilisateur est déjà utilisé') ;
                this.$view.error = errorMsg ;
              }
              else {
          
                if (vars.newPassword) user.setPassword(vars.newPassword) ; 
                
                return this.get('$models').get('users').save(user).then(() => {
                          
                  if (form.type == 'add')
                    this.$user.setFlash('Utilisateur ajouté', 'success') ;
                  else {
                    this.$user.setFlash(
                      'Modification(s) enregistrée(s)', 'success'
                    ) ;
                  }
                          
                  this.$response.redirect(this.$view.$getRoute('adminUsers')) ;
                }).catch((e) => {

                  console.error(e) ; 
                            
                  if (form.type == 'add')
                    this.$view.error = 'Erreur lors de l’ajout' ;
                  else 
                    this.$view.error =  'Erreur lors de l’enregistrement' ;
                }) ;
              }
            }) ;
          }
        }) ;
      }
    }
  }


  _circleForm (form, circle, vars) {

    let errorMsg = (form.type == 'add' ? 'Ajout' : 'Mise-à-jour') 
                 + ' impossible du cercle' ;

    // On a soumis le formulaire
    if (this.$request.method == 'POST') {

      // Modification du name
      if (vars.name !== circle.name)
        circle.setName(vars.name) ;

      if (!form.process(vars))
        this.$view.error = 'Certains champs ne sont pas valides' ;
      
      else {
 
        // unicité du name
        return this._nameExist(circle.name).then(exist => {

          if (exist && circle.id != exist) {
            form.getField('name')
                .setInvalid('Ce nom est déjà utilisé') ;
            this.$view.error = errorMsg ;
          }
          else {
          
            return this.get('$models').get('circles').save(circle).then(() => {
                          
              if (form.type == 'add')
                this.$user.setFlash('Cercle ajouté', 'success') ;
              else {
                this.$user.setFlash(
                  'Modification(s) enregistrée(s)', 'success'
                ) ;
              }
                          
              this.$response.redirect(this.$view.$getRoute('adminUsers')) ;
            }).catch((e) => {

              console.error(e) ; 
                            
              if (form.type == 'add')
                this.$view.error = 'Erreur lors de l’ajout' ;
              else 
                this.$view.error =  'Erreur lors de l’enregistrement' ;
            }) ;
          }
        }) ;
      }
    }
  }
}

AdminController.prototype.setViews = {
  circleUser: null
} ;

module.exports = AdminController ;

