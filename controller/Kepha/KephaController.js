'use strict' ;

class KephaController {

  init () {

    this.get('setSite')(this.$view, 'kepha') ;
  }

  indexAction () {

    this.$view.title = 'Kepha' ;
    this.$view.accueilActive = 'active' ;
  }


  stylesAction () {

    this.$view.title = 'Styles' ; 
    this.$view.kephaLessActive = 'active' ;

    this.$view.iconList = [] ;

    require('../../kephaless/dist/list.json').forEach(elmt => {
      this.$view.iconList.push({name: elmt}) ;
    }) ;
  }
}
module.exports = KephaController ;

