'use strict' ;

/**
 * entity/Cirlce.js
 */

const Entity = require('../kephajs').Entity ;

class Circle extends Entity {

  /**
   * Description de l'entité
   */
  _setProperties () {

    this._setProperty('id', 'integer', 'gidNumber') ;
    this._setProperty('name', 'string', 'cn') ; 
    this._setProperty('description', 'string') ;
    this._setProperty('members', null, 'memberUid', '') ;
  }


  setName (name) {

    if (name !== this.name) {
      this._hasModifications = true ; 
      this._oldName = this.name ; 
      this.name = name ;
    }
  }


  addMember (username) {

    if (this.members.indexOf(username) < 0) {
      if (Array.isArray(this.members))
        this.members.push(username) ;
      else if (this.members == '')
        this.members = username ;
      else 
        this.members = [this.members, username] ;
      this._hasModifications = true ; 
    }
  }

  
  removeMember (username) {

    let index = this.members.indexOf(username) ;

    if (this.members == username) {
      this.members = '' ;
      this._hasModifications = true ; 
    }
    if (index > -1) {
      delete this.members[index] ;
      this._hasModifications = true ; 
    }
  }


  toString () {

    return this.description ? this.description : this.name ; 
  }
}

module.exports = Circle ; 

