'use strict' ;

/**
 * entity/User.js
 */

const execSync = require('child_process').execSync ;
const Entity = require('../kephajs').Entity,
      kephajs = require('../kephajs') ;

class User extends Entity {

  /**
   * Description de l'entité
   */
  _setProperties () {

    this._newPassword = false ; 

    this._setProperty('id', 'integer', 'uidNumber') ;
    this._setProperty('username', 'string', 'uid') ; 
    this._setProperty('password', 'string', 'userPassword') ; 
    this._setProperty('mail', 'mail') ;
    this._setProperty('dateCreation', 'string', 'createTimestamp') ;
    this._setProperty('prenom', 'string', 'givenName') ;
    this._setProperty('nom', 'string', 'sn') ; 
    this._setProperty('description', 'string', null, '') ;
    this._setProperty('quota', 'string', 'carLicense') ;
    this._setProperty('avatar', null, 'jpegPhoto') ;
  }


  setUsername (username) {

    if (username !== this.username) {
      this._hasModifications = true ; 
      this._oldUsername = this.username ; 
      this.username = username ;
    }
  }


  setPassword (password) {

    this._hasModifications = true ; 
    this._newPassword = execSync('slappasswd  -h \'{SSHA}\' -s ' + password) ; 
  }


  getCircles () {

    return kephajs.kernel().get('$models').get('circles')
      .getByMember(this.username) ;
  }


  toString () {

    return this.description ? this.description : this.username ; 
  }
}

module.exports = User ; 

