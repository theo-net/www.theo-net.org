'use strict' ;

/**
 * entity/Category.js
 */

const Entity = require('../kephajs').Entity ;

class Category extends Entity {

  /**
   * Description de l'entité
   */
  _setProperties () {

    this._setProperty('id', {string: {max: 40}}) ;
    this._setProperty('name', {string: {max: 255}}) ; 
    this._setProperty('parent', {string: {max: 40}}) ;
    this._setProperty('publishers', {string: {max: 255}}) ;
    this._setProperty('authors', {string: {max: 255}}) ;

    this.getPublishers = () => {
      return this.publishers ; 
    } ;
  }

  
  toString () {

    return this.name ; 
  }
}

module.exports = Category ; 

