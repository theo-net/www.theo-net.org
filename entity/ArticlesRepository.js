'use strict' ;

/**
 * entity/ArticlesRepository.js
 */

const fs = require('fs') ;

const Utils = require('../kephajs') ;

class ArticlesRepository {

  constructor (source = '') {

    let filesDir = __dirname + '/../data/articles' + source + '/' ;
    let months = {
      0:  {short: 'janv', end: 'ier'},
      1:  {short: 'fév', end: 'rier'},
      2:  {short: 'mars', end: ''},
      3:  {short: 'avr', end: 'il'},
      4:  {short: 'mai', end: ''},
      5:  {short: 'juin', end: ''},
      6:  {short: 'jui', end: 'llet'},
      7:  {short: 'août', end: ''},
      8:  {short: 'sept', end: 'embre'},
      9: {short: 'oct', end: 'obre'},
      10: {short: 'nov', end: 'embre'},
      11: {short: 'déc', end: 'embre'},
    } ;

    this._articles = {} ;
    this._nb = 0 ;

    fs.readdir(filesDir, (err, files) => {

      if (err) throw new Error(err) ;

      files.forEach(file => {
      
        if (/^\d+\.html$/.test(file)) {
          fs.readFile(filesDir + file, (err, contentFile) => {

            if (err) throw new Error(err) ;

            contentFile = /^(.+)\n(.+)\n((?:.|\s)+)$/m
              .exec(contentFile.toString()) ; 

            let id = Number(file.replace('.html', '')),
                title = contentFile[2],
                datePubli = new Date(contentFile[1]),
                articleRender = Utils.kernel().get('$kfmd')
                                              .render(contentFile[3]), 
                dayPubli = datePubli.getDate() ;

            // On génère les notes
            let footNotes = [] ;
            if (Array.isArray(articleRender.footNotes)) {
              articleRender.footNotes.forEach(note => {
                footNotes.push({note}) ;
              }) ;
            }

            // On sauve l'article
            this._articles[id] = {
              id, title, content: articleRender.contentHtml, 
              footNotes, datePubli: {
                iso: datePubli,
                year: datePubli.getFullYear(),
                monthSort: months[datePubli.getMonth()].short,
                monthEnd: months[datePubli.getMonth()].end,
                day: (dayPubli > 9 ? dayPubli : '0' + dayPubli),
              }
            } ;

            this._nb++ ;
          }) ;
        }
      }) ;
    }) ;
  }

  getAll () {

    return this.getList() ;
  }


  /**
   * Retourne une série d'article
   * @param {Integer} [debut] À partir de quelle position on retourne la liste
   * @param {Integer} [limit] Combien d'article on retourne (mettre -1, valeur
   *    par défaut, pour retourner tous les articles depuis `debut`)
   * @returns {Array}
   */
  getList (debut = 0, limit = -1) {

    let out   = Array(),
        count = limit ; 

    if (debut < 0)
      debut = 0 ;

    Utils.forEach(this._articles, article => {

      if (count === 0)
        return false ;
      if (article.id >= debut) {
        out.push(article) ; 
        count-- ;
      }
    }) ;

    return out ;
  }

  count () {
    
    return this._nb ; 
  }
}

module.exports = ArticlesRepository ;

