'use strict' ;

/**
 * entity/AlbumsRepository.js
 */

class AlbumsRepository {

  constructor () {

    this._albums = require('../data/albums.json') ;
  }

  getAll () {

    return this._albums ;
  }
}

module.exports = AlbumsRepository ;

