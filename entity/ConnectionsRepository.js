'use strict' ; 

/**
 * entity/ConnectionRepository.js
 */

const bcrypt = require('bcrypt') ; 

const newUuid = require('../kephajs').uuid ;

/**
 * Repository des connexions persistantes qui permettent à un utilisateur de
 * se connecter automatiquement lorsqu'il accède au site (fonction « se souvenir
 * de moi »). Pour cela un cookie est transmis à l'utilisateur et on compare
 * différentes informations, notamment certaines permettant d'identifier le 
 * navigateur. 
 */
class ConnectionsRepository {

  /**
   * Initialise le repository. Un `garbage` est lancé automatiquement.
   * @param {Mysql} mysql Connexion MySQL, depuis le service du même nom
   */
  constructor (mysql) {

    this._mysql = mysql ;

    bcrypt.genSalt(10, (err, salt) => {
      if (err)
        throw new Error(err) ; 

      this._salt = salt ; 
    }) ; 

    this.garbage().catch(e => { console.error(e) ; }) ;
  }


  /**
   * Enregistre une connexion. Un token est renvoyé, celui-ci n'est pas stocké
   * en db, mais il est utilisé, avec les infos du navigateur pour créer un 
   * hash qui sera stocké en db est servira de point de repère pour la 
   * comapraison.
   * @param {String} browser Infos sur le navigateur
   * @param {String} username Nom d'utilisateur
   * @returns {Promise} Lors de la résolution transmet les données que le
   *    cookie doit sauver (`id:::token`)
   */
  store (browser, username) {

    let id    = newUuid(),
        token = newUuid() ;

    return new Promise((resolve, reject) => {
      
      bcrypt.hash(token + browser, this._salt, (err, hash) => {

        if (err)
          reject(err) ;

        this._mysql.query(
          'INSERT INTO persistConnections VALUES(?, ?, ?, NOW())',
          [id, hash, username]
        ).then(() => {
          resolve(id + ':::' + token) ;
        }) 
        .catch((e) => {
          reject('Impossible d\'enregistrer la connexion persistante : ' + e) ;
        }) ;
      }) ;
    }) ;
  }


  /**
   * Compare des données avec ce qui est stocké dans la base de données. Permet
   * la connexion de l'utilisateur. On compare le token et les infos du navigateur
   * avec le hash contenu dans la db. On vérifie également que l'élément stocké en
   * base de données n'est pas trop ancien (ie n'a pas expiré), puis si tout est
   * ok, on supprime la ligne : un nouvel élément devra être créé pour la prochaine
   * connexion de notre utilisateur. 
   * @param {String} id Identifiant transmis par le cookie
   * @param {String} token Token stocké dans le cookie
   * @param {String} browser Informations du navigateur
   * @returns {Promise} La résolution transmet le nom d'utilisateur qui servira 
   *    pour établir la connexion.
   */
  compare (id, token, browser) {

    return new Promise((resolve, reject) => {

      this._mysql.query(
        'SELECT id, hash, username, time FROM persistConnections '
        + 'WHERE id = ? AND time > ?',
        [id, this._expireDate()]
      ).then(results => {

        if (!Array.isArray(results) || results.length != 1)
          reject() ;
        else {

          bcrypt.compare(token + browser, results[0].hash, 
          (err, res) => {
       
            if (err || !res)
              reject() ;
            else {

              let username = results[0].username ;
              this._mysql.query(
                'DELETE FROM persistConnections WHERE id = ?',
                results[0].id
              ) ;
          
              resolve(username) ;
            }
          }) ;
        }
      }) ;
    }) ;
  }


  /**
   * Supprime une connexion persistante à partir de son `id`
   * @param {String} id Identifiant
   * @returns {Promise}
   */
  delete (id) {

    return this._mysql.query(
      'DELETE FROM persistConnections WHERE id = ?',
      [id]
    ) ;
  }


  /**
   * Lance un `garbage` permettant de supprimer toutes les vieilles entrées
   * @returns {Promise}
   */
  garbage () {

    return this._mysql.query(
      'DELETE FROM persistConnections WHERE time < ?',
      [this._expireDate()]
    ) ;
  }


  /**
   * Retourne le nombre de connexions persistantes stockées dans la db. Un 
   * garbage est lancé juste avant. 
   * @returns {Promise}
   */
  count () {

    return new Promise((resolve, reject) => {
      
      this.garbage().then(() => {

        this._mysql.query('SELECT COUNT(*) AS nb FROM persistConnections')
        .then(results => { 

          if (!Array.isArray(results) || results.length != 1)
            reject() ;
          else
            resolve(results[0].nb) ;
        }).catch(e => reject(e)) ;
      }).catch(e => reject(e)) ;
    }) ;
  }


  /**
   * Retourne la date la plus ancienne que peut avoir un élément pour être
   * valide.
   * @returns {Date}
   */
  _expireDate () {

    let expire = new Date() ;
    expire.setDate(expire.getDate() - 30) ;
    return expire ;
  }
}

module.exports = ConnectionsRepository ; 

