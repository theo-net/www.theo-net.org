'use strict' ; 

/**
 * entity/CirclesRepository.js
 */

const Circle = require('./Circle') ;

class CirclesRepository {

  constructor (ldap) {

    this._ldap = ldap ; 

    this._allAttributes = [
      'gidNumber', 'createTimestamp', 'cn', 'description', 'memberUid'
    ] ;
  }


  getAll () {

    return this._ldap.search('(&(objectClass=posixGroup)(gidNumber=*))', 
      this._allAttributes, Circle
    ) ;
  }


  getById (gidNumber) {

    return this._ldap
      .search('(&(objectClass=posixGroup)(gidNumber=' + gidNumber + '))', 
        this._allAttributes, Circle
      ).then(results => { return results[0] ; }) ;
  }


  getByCn (cn) {

    cn = this._ldap.escape('dn', cn) ;
    return this._ldap.search('(&(objectClass=posixGroup)(cn=' + cn + '))', 
      this._allAttributes, Circle
    ).then(results => { return results[0] ; }) ;
  }


  getByMember (memberUid) {

    return this._ldap
      .search('(&(objectClass=posixGroup)(memberUid=' + memberUid + '))', 
        this._allAttributes, Circle
      ) ;
  }


  count () {

    return this._ldap
      .search('(&(objectClass=posixGroup)(gidNumber=*))', ['gidNumber'])
      .then(results => { return results.length ; }) ;
  }


  save (circle) {

    if (circle.isNew() || circle.hasModifications()) {

      // Préparation des données
      let entry = {
        gidNumber: circle.gid, 
        cn: circle.name,
        description: circle.description, 
        memberUid: circle.members
      } ;

      // On sauve (selon que c'est modif ou nouveau
      // Si c'est nouveau, il faudra créer un nouvel id que l'on donnera aussi
      // à l'objet.

      return new Promise((resolve, reject) => { 
       
        // C'est une modification
        if (!circle.isNew()) {

          let changes = [
            this._ldap.change({
              operation: 'replace', 
              modification: {description: entry.description}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {memberUid: entry.memberUid}
            }),
          ] ;

          if (circle._oldName) {
          
            this._ldap.modifyDN('cn=' + circle._oldName + ',cn=users', 
            'cn=' + entry.cn + ',cn=users').catch(e => {
              reject(e) ; 
            }) ;
          }

          this._ldap.modify('cn=' + entry.cn + ',cn=users', changes)
          .then(() => {

            circle.saved() ;
            resolve() ; 
          }).catch(e => { 
            reject(e) ; 
          }) ;
        }
        // C'est une nouvelle entrée
        else {

          // Ajouter UID
          this.getLastGid().then(gid => { 
            
            entry.gidNumber = gid + 1 ;
            entry.objectclass = ['posixGroup', 'top'] ;
       
            this._ldap.add('cn=' + entry.cn + ',cn=users', entry).then(() => {

              circle.saved() ;
              resolve() ; 
            }).catch(e => { 
              reject(e) ; 
            }) ;
          }) ;
        }
      }) ;
    }

    return new Promise(resolve => { resolve() ; }) ;
  }


  getLastGid () {

    return this.getAll().then(results => {

      let maxGid = 0 ; 
      results.forEach(entry => {

        if (maxGid < entry.id)
          maxGid = entry.id ;
      }) ;

      return new Number(maxGid) ;
    }) ; 
  }


  /**
   * Supprime, définitivement, un cercle
   * @param {User} circle Cercle à supprimer 
   * @returns {Promise}
   */
  delete (circle) {

    return this._ldap.delete('cn=' + circle.name + ',cn=users') ;
  }
}

module.exports = CirclesRepository ; 

