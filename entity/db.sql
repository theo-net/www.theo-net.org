--
-- Base de données :  `tn_network_db`
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publishers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ;

-- --------------------------------------------------------

--
-- Structure de la table `persistConnections`
--

CREATE TABLE `persistConnections` (
  `id` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `hash` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) ;

--
-- Index pour la table `persistConnections`
--
ALTER TABLE `persistConnections`
  ADD PRIMARY KEY (`id`) ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `publishers`, `authors`) VALUES
('3680a45b-f4b7-438a-acec-9a8781901c8b', 'Terre Sainte', NULL, NULL, NULL),
('82761ac8-b97f-49ee-a8c0-fe1177b5b1d8', 'Fraternité Saint Étienne', NULL, NULL, NULL) ;

