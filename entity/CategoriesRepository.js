'use strict' ; 

/**
 * entity/CategoriesRepository.js
 */

const newUuid  = require('../kephajs').uuid,
      kephajs  = require('../kephajs'),
      Category = require('./Category') ;

class CategoriesRepository {

  /**
   * Initialise le repository. 
   * @param {Mysql} mysql Connexion MySQL, depuis le service du même nom
   */
  constructor (mysql) {

    this._mysql = mysql ;
  }


  /**
   * Retourne l'intégralité des catégories
   * @returns {Promise}
   */
  getAll () {

    return new Promise((resolve, reject) => {

      this._mysql.query(
        'SELECT * FROM categories'
      ).then(result => {
        resolve(result.map(category => new Category(category))) ;
      }).catch(e => {
        reject('Erreur MySQL : ' + e) ;
      }) ;
    }) ;
  }


  /**
   * Retourne tous les éléments sous forme d'arbre
   * @returns {Promise}
   */
  getAllTree () {

    return this.getAll().then(result => {

      let categories = {} ;
      result.forEach(category => {
        categories[category.id] = {main: category, subs: {}} ;
      }) ;

      let tree = {} ;

      kephajs.forEach(categories, (category, id) => {
        if (category.main.parent === null || category.main.parent === '')
          tree[id] = category ;
        else
          categories[category.main.parent].subs[id] = category ;
      }) ;
      return Promise.resolve(tree) ;
    }) ;
  }


  /**
   * Récupère un élément à partir de son id
   * @param {String} id Identifiant de la catégorie
   * @returns {Promise}
   */
  getById (id) {

    return new Promise((resolve, reject) => {

      this._mysql.query(
        'SELECT * FROM categories WHERE id = ?', id
      ).then(result => {
        if (result.length == 0)
          resolve(undefined) ;
        else
          resolve(new Category(result[0])) ;
      }).catch(e => {
        reject('Erreur MySQL : ' + e) ;
      }) ;
    }) ;
  }


  /**
   * Sauve un élément
   * @param {Category} category Élément à sauvegarder
   * @returns {Promise}
   */
  save (category) {

    if (category.hasModifications()) {

      if (category.isNew()) {
   
        category.id = newUuid() ;

        return this._mysql.query(
          'INSERT INTO categories VALUES(?, ?, ?, ?, ?)', [
            category.id, category.name, category.parent, 
            category.publishers, category.authors
          ]).then(() => {
          category.saved() ;
          return Promise.resolve() ;
        }) ;
      }
      else {

        return this._mysql.query(
          'UPDATE categories '
          + 'SET name = ?, parent = ?, publishers = ?, authors = ? '
          + 'WHERE id = ?', [
            category.name, category.parent, category.publishers, 
            category.authors, category.id
          ]).then(() => {
          category.saved() ;
          return Promise.resolve() ;
        }) ;
      }
    }
  }
}

module.exports = CategoriesRepository ; 

