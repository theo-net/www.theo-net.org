'use strict' ; 

/**
 * entity/UsersRepository.js
 */

const fs = require('fs') ;
const kephajs = require('../kephajs'),
      User    = require('./User') ;

class UsersRepository {

  constructor (ldap) {

    this._ldap = ldap ; 

    this._allAttributes = [
      'uidNumber', 'uid', 'mail', 'createTimestamp', 'givenName', 'sn', 
      'description', 'carLicense', 'userPassword'
    ] ;
  }


  getAll () {

    return this._ldap.search('(&(objectClass=posixAccount)(uid=*))', 
      this._allAttributes, User
    ) ;
  }


  getById (uidNumber) {

    uidNumber = this._ldap.escape('filter', uidNumber) ;
    return this._ldap
      .search('(&(objectClass=posixAccount)(uidNumber=' + uidNumber + '))', 
        this._allAttributes, User
      ).then(results => { return results[0] ; }) ;
  }


  getByCn (cn) {

    cn = this._ldap.escape('dn', cn) ;
    return this._ldap.search('(&(objectClass=posixAccount)(cn=' + cn + '))', 
      this._allAttributes, User
    ).then(results => { return results[0] ; }) ;
  }


  getByMail (mail) {

    mail = this._ldap.escape('filter', mail) ;
    return this._ldap
      .search('(&(objectClass=posixAccount)(mail=' + mail + '))', 
        this._allAttributes, User
      ).then(results => { return results[0] ; }) ;
  }


  count () {

    return this._ldap.search('(&(objectClass=posixAccount)(uid=*))', ['uid'])
      .then(results => { return results.length ; }) ;
  }


  save (user) {

    if (user.isNew() || user.hasModifications()) {

      // Préparation des données
      let entry = {
        uid: this._ldap.escape('dn', user.username), 
        cn: this._ldap.escape('dn', user.username), 
        mail: this._ldap.escape('filter', user.mail), 
        givenName: this._ldap.escape('filter', user.prenom), 
        sn: this._ldap.escape('filter', user.nom),
        carLicense: this._ldap.escape('filter', user.quota),
        homeDirectory: 
          this._ldap.escape('filter', '/home/users/' + user.username),
      } ;

      // Il y a une description
      if (user.description)
        entry.description = this._ldap.escape('filter', user.description) ;

      // Password modifié
      if (user._newPassword) 
        entry.userPassword = user._newPassword ; 

      // On sauve (selon que c'est modif ou nouveau
      // Si c'est nouveau, il faudra créer un nouvel id que l'on donnera aussi
      // à l'objet.

      return new Promise((resolve, reject) => { 
       
        // C'est une modification
        if (!user.isNew()) {

          let changes = [
            this._ldap.change({
              operation: 'replace', 
              modification: {uid: entry.uid}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {cn: entry.cn}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {mail: entry.mail}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {givenName: entry.givenName}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {sn: entry.sn}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {carLicense: entry.carLicense}
            }),
            this._ldap.change({
              operation: 'replace', 
              modification: {homeDirectory: entry.homeDirectory}
            }),
          ] ;

          if (entry.description) {
            changes.push(
              this._ldap.change({
                operation: 'replace', 
                modification: {description: entry.description}
              })
            ) ;
          }

          if (entry.userPassword) {
            changes.push(
              this._ldap.change({
                operation: 'replace', 
                modification: {userPassword: entry.userPassword}
              })
            ) ;
          }

          if (user._oldUsername) {
          
            this._ldap.modifyDN('cn=' + user._oldUsername + ',cn=users', 
            'cn=' + entry.cn + ',cn=users').catch(e => {
              reject(e) ; 
            }) ;
          }

          this._ldap.modify('cn=' + entry.cn + ',cn=users', changes)
          .then(() => {

            user.saved() ;
            resolve() ; 
          }).catch(e => { 
            reject(e) ; 
          }) ;
        }
        // C'est une nouvelle entrée
        else {

          // Ajouter UID
          this.getLastUid().then(uid => { 
            
            entry.uidNumber = uid + 1 ;
            entry.gidNumber = 501 ;
            entry.objectclass = ['inetOrgPerson', 'posixAccount', 'top'] ;
          
            this._ldap.add('cn=' + entry.cn + ',cn=users', entry).then(() => {

              user.saved() ;
              resolve() ; 
            }).catch(e => { 
              reject(e) ; 
            }) ;
          }) ;
        }
      }) ;
    }

    return new Promise(resolve => { resolve() ; }) ;
  }


  getLastUid () {

    return this.getAll().then(results => {

      let maxUid = 0 ; 
      results.forEach(entry => {

        if (maxUid < entry.id)
          maxUid = entry.id ;
      }) ;

      return new Number(maxUid) ;
    }) ; 
  }


  /**
   * Supprime, définitivement, un utilisateur
   * @param {User} user Utilisateur à supprimer
   * @returns {Promise}
   */
  delete (user) {

    return this._ldap.delete('cn=' + user.username + ',cn=users') ;
  }


  /**
   * Charge l'avatar d'un utilisateur
   * @param {String} username Nom d'utilsiateur
   * @returns {Promise} `resolve` avec le chemin vers l'avatar ou `null` si
   *    aucun n'est associé. `reject` avec l'erreur en cas de problèmes.
   */
  loadAvatar (username) {

    return this._ldap.search(
      '(&(objectClass=posixAccount)(uid=' + username + '))', 
      ['jpegPhoto'], 'raw'
    ).then(results => {
    
      return new Promise((resolve, reject) => {
      
        if (results[0] && results[0].jpegPhoto) {

          let filename = 'tmp/cache/avatar/avatar-' + username + '.jpg' ;
          let file = __dirname + '/../' + filename ;
          fs.writeFile(file, results[0].jpegPhoto, error => {
            if (error) reject(error) ;
            else resolve(filename) ;
          }) ;
        }
        else
          resolve(null) ;
      }) ;
    }) ;
  }


  /**
   * Inititialise le répertoire de cache pour les avatars
   */
  initCacheDir () {

    kephajs.rmrf(__dirname + '/../tmp/cache/avatar', error => {
      if (error && error.code != 'ENOENT')
        throw error ;
      fs.mkdir(__dirname + '/../tmp/cache/avatar', error => {
        if (error && error.code != 'EEXIST')
          throw error ;
      }) ;
    }) ;
  }
}

module.exports = UsersRepository ; 

